#use wml::debian::translation-check translation="9d41ab1625a3bbe9bf95b782d91e91b766a3f664"
<define-tag pagetitle>Обновлённый Debian 11: выпуск 11.9</define-tag>
<define-tag release_date>2024-02-10</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.9</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает о девятом обновлении своего
предыдущего стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Исправления различных ошибок</h2>

<p>Данное предыдущее стабильное обновление вносит несколько важных исправлений для следующих пакетов:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction axis "Фильтрация неподдерживаемых протоколов в клиентском классе ServiceFactory [CVE-2023-40743]">
<correction base-files "Обновление для редакции 11.9">
<correction cifs-utils "Исправление непараллельных сборок">
<correction compton "Удаление рекомендации пакета picom">
<correction conda-package-handling "Пропуск ненадёжных тестов">
<correction conmon "Предотвращение зависания при пересылке контейнера stdout/stderr с большим количество выводных данных">
<correction crun "Исправление контейнеров с systemd в качестве системы инициализации при использовании новых версий ядра">
<correction debian-installer "Увеличение ABI ядра Linux до 5.10.0-28; повторная сборка с учётом proposed-updates">
<correction debian-installer-netboot-images "Повторная сборка с учётом proposed-updates">
<correction debian-ports-archive-keyring "Добавление ключа Debian Ports Archive Automatic Signing Key (2025)">
<correction debian-security-support "Отметка пакетов tor, consul и xen как завершивших жизненный цикл; ограничение поддержки samba до случаев без AD DC; выборка пакетов golang по регулярному выражению; отказ от проверки версий; добавление chromium в security-support-ended.deb11; добавление tiles и libspring-java в security-support-limited">
<correction debootstrap "Обратный перенос изменений поддержки merged-/usr из trixie: реализация merged-/usr путём слияния после изменений, по умолчанию merged-/usr подходит для всех профилей наборов новее bookworm">
<correction distro-info "Обновление тестов для distro-info-data 0.58+deb12u1, что уточняет дату окончания жизненного цикла Debian 7">
<correction distro-info-data "Добавление Ubuntu 24.04 LTS Noble Numbat; исправление нескольких дат окончания жизненного цикла">
<correction dpdk "Новый стабильный выпуск основной ветки разработки">
<correction dropbear "Исправление обхода мер безопасности [CVE-2021-36369]; исправление атаки <q>terrapin</q> [CVE-2023-48795]">
<correction exuberant-ctags "Исправление выполнения произвольных команд [CVE-2022-4515]">
<correction filezilla "Предотвращение эксплоита <q>terrapin</q> [CVE-2023-48795]">
<correction gimp "Удаление старых версий дополнения dds, которые ранее поставлялись в отдельном пакете">
<correction glib2.0 "Добавление исправлений из стабильного выпуска основной ветки разработки; исправление отказа в обслуживании [CVE-2023-32665 CVE-2023-32611 CVE-2023-29499 CVE-2023-32636]">
<correction glibc "Исправление повреждения содержимого памяти в функции <q>qsort()</q> при использовании нетранзитивных функций сравнения.">
<correction gnutls28 "Исправление проблемы с таймингом, доступной через сторонний канал [CVE-2023-5981]">
<correction imagemagick "Различные исправления безопасности [CVE-2021-20241 CVE-2021-20243 CVE-2021-20244 CVE-2021-20245 CVE-2021-20246 CVE-2021-20309 CVE-2021-3574 CVE-2021-39212 CVE-2021-4219 CVE-2022-1114 CVE-2022-28463 CVE-2022-32545 CVE-2022-32546]">
<correction jqueryui "Исправление межсайтового скриптинга [CVE-2022-31160]">
<correction knewstuff "Проверка правильности ProvidersUrl для исправления отказа в обслуживании">
<correction libdatetime-timezone-perl "Обновление поставляемых данных и временных зонах">
<correction libde265 "Исправление ошибки сегментирования в функции <q>decoder_context::process_slice_segment_header</q> [CVE-2023-27102]; исправление переполнения динамической памяти в функции <q>derive_collocated_motion_vectors</q> [CVE-2023-27103]; исправление чтения за пределами выделенного буфера памяти в <q>pic_parameter_set::dump</q> [CVE-2023-43887]; исправление переполнения буфера в функции <q>slice_segment_header</q> [CVE-2023-47471]; исправление переполнения буфера [CVE-2023-49465 CVE-2023-49467 CVE-2023-49468]">
<correction libmateweather "Обновление поставляемых данных о местоположении; обновление URL сервера данных">
<correction libpod "Исправление неправильный обработки дополнительных групп [CVE-2022-2989]">
<correction libsolv "Включение поддержки сжатия zstd">
<correction libspreadsheet-parsexlsx-perl "Исправление возможной бомбы памяти [CVE-2024-22368]; исправление ошибки с внешней сущностью XML [CVE-2024-23525]">
<correction linux "Новый стабильный выпуск основной ветки разработки; увеличение ABI до 28">
<correction linux-signed-amd64 "Новый стабильный выпуск основной ветки разработки; увеличение ABI до 28">
<correction linux-signed-arm64 "Новый стабильный выпуск основной ветки разработки; увеличение ABI до 28">
<correction linux-signed-i386 "Новый стабильный выпуск основной ветки разработки; увеличение ABI до 28">
<correction llvm-toolchain-16 "Новый пакет для поддержки сборки новых версий chromium; сборочная зависимость от <q>llvm-spirv</q> вместо <q>llvm-spirv-16</q>">
<correction mariadb-10.5 "Новый стабильный выпуск основной ветки разработки; исправление отказа в обслуживании [CVE-2023-22084]">
<correction minizip "Отброс переполнений полей заголовков zip [CVE-2023-45853]">
<correction modsecurity-apache "Исправление обхода защиты [CVE-2022-48279 CVE-2023-24021]">
<correction nftables "Исправление порождения байткода">
<correction node-dottie "Исправление загрязнения прототипа [CVE-2023-26132]">
<correction node-url-parse "Исправление обхода авторизации [CVE-2022-0512]">
<correction node-xml2js "Исправление загрязнения прототипа [CVE-2023-0842]">
<correction nvidia-graphics-drivers "Новый выпуск основной ветки разработки [CVE-2023-31022]">
<correction nvidia-graphics-drivers-tesla-470 "Новый выпуск основной ветки разработки [CVE-2023-31022]">
<correction opendkim "Корректное удаление заголовков Authentication-Results [CVE-2022-48521]">
<correction perl "Предотвращение переполнения буфера из-за некорректного свойства Unicode [CVE-2023-47038]">
<correction plasma-desktop "Исправление отказа в обслуживании в службе discover">
<correction plasma-discover "Исправление отказа в обслуживании; исправление ошибки сборки">
<correction postfix "Новый стабильный выпуск основной ветки разработки; решение проблемы с подделкой SMTP [CVE-2023-51764]">
<correction postgresql-13 "Новый стабильный выпуск основной ветки разработки; исправление SQL-инъекции [CVE-2023-39417]">
<correction postgresql-common "Исправление автоматических тестов">
<correction python-cogent "Пропуск тестов на системах с одним ЦП">
<correction python-django-imagekit "Предотвращение обнаружения обхода пути в тестах">
<correction python-websockets "Исправление предсказуемого времени исполнения [CVE-2021-33880]">
<correction pyzoltan "Сборка на системах с одним ядром">
<correction ruby-aws-sdk-core "Добавление в пакет файла VERSION">
<correction spip "Исправление межсайтового скриптинга">
<correction swupdate "Предотвращение получения прав суперпользователя через несоответствующий режим сокета">
<correction symfony "Правильное экранирование входных данных фильтров CodeExtension [CVE-2023-46734]">
<correction tar "Исправление проверки границ массива в декодере base-256 [CVE-2022-48303], обработка префиксов расширенных заголовков [CVE-2023-39804]">
<correction tinyxml "Исправление ошибки утверждения [CVE-2023-34194]">
<correction tzdata "Обновление поставляемых данных о временных зонах">
<correction unadf "Исправление переполнения стека [CVE-2016-1243]; исправление произвольного выполнения кода [CVE-2016-1244]">
<correction usb.ids "Обновление поставляемого списка данных">
<correction vlfeat "Исправление FTBFS с новыми версиями ImageMagick">
<correction weborf "Исправление отказа в обслуживании">
<correction wolfssl "Исправление переполнения буфера [CVE-2022-39173 CVE-2022-42905], раскрытия ключа [CVE-2022-42961], предсказуемого буфера в вводном материале ключа [CVE-2023-3724]">
<correction xerces-c "Исправление использования указателей после освобождения памяти [CVE-2018-1311]; исправление переполнения целых чисел [CVE-2023-37536]">
<correction zeromq3 "Исправление обнаружения <q>fork()</q> при использовании gcc 7; обновление информации о перелицензировании">
</table>


<h2>Обновления безопасности</h2>


<p>В данный выпуск внесены следующие обновления безопасности. Команда
безопасности уже выпустила рекомендации для каждого
из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2023 5496 firefox-esr>
<dsa 2023 5499 chromium>
<dsa 2023 5506 firefox-esr>
<dsa 2023 5508 chromium>
<dsa 2023 5509 firefox-esr>
<dsa 2023 5511 mosquitto>
<dsa 2023 5512 exim4>
<dsa 2023 5513 thunderbird>
<dsa 2023 5514 glibc>
<dsa 2023 5515 chromium>
<dsa 2023 5516 libxpm>
<dsa 2023 5517 libx11>
<dsa 2023 5518 libvpx>
<dsa 2023 5519 grub-efi-amd64-signed>
<dsa 2023 5519 grub-efi-arm64-signed>
<dsa 2023 5519 grub-efi-ia32-signed>
<dsa 2023 5519 grub2>
<dsa 2023 5520 mediawiki>
<dsa 2023 5522 tomcat9>
<dsa 2023 5523 curl>
<dsa 2023 5524 libcue>
<dsa 2023 5526 chromium>
<dsa 2023 5527 webkit2gtk>
<dsa 2023 5528 node-babel7>
<dsa 2023 5530 ruby-rack>
<dsa 2023 5531 roundcube>
<dsa 2023 5533 gst-plugins-bad1.0>
<dsa 2023 5534 xorg-server>
<dsa 2023 5535 firefox-esr>
<dsa 2023 5536 chromium>
<dsa 2023 5537 openjdk-11>
<dsa 2023 5538 thunderbird>
<dsa 2023 5539 node-browserify-sign>
<dsa 2023 5540 jetty9>
<dsa 2023 5542 request-tracker4>
<dsa 2023 5543 open-vm-tools>
<dsa 2023 5544 zookeeper>
<dsa 2023 5545 vlc>
<dsa 2023 5546 chromium>
<dsa 2023 5547 pmix>
<dsa 2023 5548 openjdk-17>
<dsa 2023 5549 trafficserver>
<dsa 2023 5550 cacti>
<dsa 2023 5551 chromium>
<dsa 2023 5554 postgresql-13>
<dsa 2023 5556 chromium>
<dsa 2023 5557 webkit2gtk>
<dsa 2023 5558 netty>
<dsa 2023 5560 strongswan>
<dsa 2023 5561 firefox-esr>
<dsa 2023 5563 intel-microcode>
<dsa 2023 5564 gimp>
<dsa 2023 5565 gst-plugins-bad1.0>
<dsa 2023 5566 thunderbird>
<dsa 2023 5567 tiff>
<dsa 2023 5569 chromium>
<dsa 2023 5570 nghttp2>
<dsa 2023 5571 rabbitmq-server>
<dsa 2023 5572 roundcube>
<dsa 2023 5573 chromium>
<dsa 2023 5574 libreoffice>
<dsa 2023 5576 xorg-server>
<dsa 2023 5577 chromium>
<dsa 2023 5579 freeimage>
<dsa 2023 5581 firefox-esr>
<dsa 2023 5582 thunderbird>
<dsa 2023 5584 bluez>
<dsa 2023 5585 chromium>
<dsa 2023 5586 openssh>
<dsa 2023 5587 curl>
<dsa 2023 5588 putty>
<dsa 2023 5590 haproxy>
<dsa 2023 5591 libssh>
<dsa 2023 5592 libspreadsheet-parseexcel-perl>
<dsa 2024 5594 linux-signed-amd64>
<dsa 2024 5594 linux-signed-arm64>
<dsa 2024 5594 linux-signed-i386>
<dsa 2024 5594 linux>
<dsa 2024 5595 chromium>
<dsa 2024 5597 exim4>
<dsa 2024 5598 chromium>
<dsa 2024 5599 phpseclib>
<dsa 2024 5600 php-phpseclib>
<dsa 2024 5602 chromium>
<dsa 2024 5603 xorg-server>
<dsa 2024 5604 openjdk-11>
<dsa 2024 5605 thunderbird>
<dsa 2024 5606 firefox-esr>
<dsa 2024 5608 gst-plugins-bad1.0>
<dsa 2024 5613 openjdk-17>
<dsa 2024 5614 zbar>
<dsa 2024 5615 runc>
</table>


<h2>Удалённые пакеты</h2>

<p>Следующие пакеты были удалены из-за причин, на которые мы не можем повлиять:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction gimp-dds "Включён в пакет gimp >=2.10">

</table>

<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию предыдущего стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной редакции:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий предыдущий стабильный выпуск:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Предлагаемые обновления для предыдущего стабильного выпуска:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Информация о предыдущем стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>
