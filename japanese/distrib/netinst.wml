#use wml::debian::template title="インターネット経由の Debian のインストール" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="f917b9adf4a1c15cca8405e010043d380e4b1b83"


<p>この方法による Debian のインストールでは、インストール<em>作業時</em>に
インターネットへの接続が機能している必要があります。
ダウンロードするデータ量は必要に応じて取得するため、
他の方法と比べて少なくて済みます。
イーサネットか無線経由でのアクセスをサポートしています。
残念ながら内蔵型の ISDN カードはサポートして<em>いません</em>。</p>

<p>ネットワーク経由のインストール方法は、3つあります。</p>

<toc-display />
<div class="line">
<div class="item col50">

<toc-add-entry name="smallcd">小さな CD または USB メモリ</toc-add-entry>

<p>以下にイメージファイルを挙げています。
プロセッサのアーキテクチャを選択してください。</p>

<stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<p>詳細については<a href="../CD/netinst/">最小の CD を使って、ネットワークインストールする</a>を参照してください。</p>

<div class="line">
<div class="item col50">

<toc-add-entry name="verysmall">最小の CD、各種 USB メモリなど</toc-add-entry>

<p>USB スティックや類似デバイスに適する小さなサイズのイメージファイルをダウンロードして、
それをメディアに書き込み、そこから起動することでインストールを開始できます。</p>

<p>各アーキテクチャ間で、種々の小さなイメージからインストールする上での
サポートにいくらか違いがあります。</p>

<p>詳細については、
<a href="$(HOME)/releases/stable/installmanual">あなたのアーキテクチャ向けのインストールガイド</a>、
特に<q>システムインストールメディアの入手</q>の章を参照してください。</p>

<p>
次に、利用可能なイメージファイルへのリンクを示します
(詳細については MANIFEST ファイルを参照してください)。
</p>

<stable-verysmall-images />
</div>
<div class="item col50 lastcol">

<toc-add-entry name="netboot">ネットワークブート</toc-add-entry>

<p>ローカルネットワークのマシンにインストールメディアを提供する
TFTP および DHCP (あるいは BOOTP や RARP) サーバをセットアップします。
クライアントのマシンの BIOS がサポートしているなら、ネットワークから (PXE と TFTP を使って) Debian インストールシステムを起動して、ネットワークから Debian の残りの部分のインストールを進められます。</p>

<p>すべてのマシンでネットワーク越しの起動がサポートされているとは限りません。
付加作業が必要となるために、この Debian インストール方法は初心者ユーザにはお勧めできません。</p>

<p>詳細については、
<a href="$(HOME)/releases/stable/installmanual">あなたのアーキテクチャ向けのインストールガイド</a>、
特に<q>TFTP ネットブート用ファイルの準備</q>の章を参照してください。</p>

<p>次にイメージファイルへのリンクを示します (詳細については MANIFEST
ファイルを参照してください)。</p>

<stable-netboot-images />
</div>
</div>

<div id="firmware_nonfree" class="important">
<p>
あなたのシステムがデバイスドライバーと一緒に<strong>non-freeのファームウェアが読み込まれている必要がある</strong>なら、
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">
汎用のファームウェアパッケージを含んだtarアーカイブ</a> を利用するか これらの
<strong>non-free</strong> なファームウェアを含んだ <strong>非公式の</strong> イメージをダウンロードできます。

tarアーカイブの利用方法とインストール時にファームウェアを読み込ませるための一般的な情報は
<a href="../releases/stable/amd64/ch06s04">インストールガイド</a>にあります。</p>
<p>

<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">
ファームウェアを含んだ<q>安定版</q>の非公式なインストールイメージ</a>
</p>
</div>
