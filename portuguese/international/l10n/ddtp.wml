#use wml::debian::template title="Projeto de Tradução de Descrições Debian &mdash; DDTP"
#use wml::debian::toc
#use wml::debian::translation-check translation="0bc9cf0a839d304e8615fd04426caf3bd6af63de"

<p>
O <a href="https://ddtp.debian.org">Projeto de Tradução de Descrições Debian</a>,
Debian Description Translation Project - DDTP em inglês (que foi implementado
por <a href="mailto:Michael%20Bramer%20%3Cgrisu@debian.org%3E">Michael Bramer</a>)
tem por objetivo prover descrições traduzidas de pacotes e prover a
infraestrutura para apoiar essas traduções. Mesmo já existindo por muitos anos,
o projeto foi desativado depois da quebra de um servidor Debian e atualmente
possui funcionalidades básicas se comparado ao que era antes.
</p>

<p>
O projeto suporta:
</p>
<ul>
  <li>Pesquisa e recuperação de descrições de pacotes atuais (sid) e mais antigos.</li>
  <li>Reutilização de parágrafos já traduzidos de descrições de um pacote
      para outro pacote.</li>
  <li>Fornecimento de arquivos <tt>Translation-*</tt> para espelhos e APT.</li>
</ul>

<p>
As
seções non-free (não livre) e non-free-firmware (firmware-não-livre) do
repositório Debian não são traduzíveis nesse momento porque poderiam existir
problemas de licenciamento, por
<!-- Eu achava que até mesmo para projetos non-free o empacotamento debian
     (debian/) é (seria) livre, mas parece que não é requisitado!? -->
exemplo, relacionados à tradução e à necessidade de verificações mais
cuidadosas.
</p>

<p>
Traduzir mais de 71000 descrições de pacotes é um grande desafio. Por favor,
nos ajude a cumprir com este objetivo. Veja nossa
<a href="#todo">lista de tarefas</a> para questões pendentes.
</p>

<toc-display/>

<toc-add-entry>Interfaces para o DDTP</toc-add-entry>

<p>
Como todas as interfaces utilizam o mecanismo DDTP, você primeiro precisa se
assegurar de que seu idioma já é suportado. Isso já deve ser o caso para grande
parte dos idiomas. Se o suporte ainda não existir, escreva para
<email debian-i18n@lists.debian.org> ou
<a href="https://salsa.debian.org/l10n-team/ddtp/-/issues">crie uma issue no Salsa</a>
 para que seu idioma possa ser ativado.</p>

<h3 id="DDTSS">A interface web</h3>
<p>
Existe uma interface web bacana
chamada <a href="https://ddtp.debian.org/ddtss/index.cgi/xx">DDTSS</a>,
desenvolvida por <a href="mailto:Martijn%20van%20Oosterhout%20%3Ckleptog@gmail.com%3E">Martijn van
Oosterhout</a>, que tenta simplificar as tarefas de tradução e revisão.
</p>

<h4>Visão geral</h4>
<p>
Suporta contribuições de traduções e adicionalmente permite
revisão de texto. Suporta configuração personalizada para cada time de tradução,
de modo que cada time pode decidir quantas revisões são necessárias até que a
descrição seja transferida para o DDTP. Também é possível pedir autorização
de usuário(a) para que somente um grupo restrito de pessoas possa realizar
certas ações. Também não é necessário se preocupar com codificações, o DDTSS
trata disso por você.
</p>

<p>
Propriedades padrão atuais:
</p>
<dl>
  <dt>número de revisões: </dt><dd>3</dd>
  <dt>idiomas suportados: </dt><dd>todos do DDTP</dd>
  <dt>autorização de usuário(a): </dt><dd>não, é aberto para todos(as)</dd>
</dl>

<p>
É possível definir uma lista padronizada de palavras para um idioma. A lista é
utilizada para prover traduções padrões através de janelas pop-up com dicas.
Está disponível via um link na parte inferior da página de idioma.
</p>

<h4>Fluxo de trabalho</h4>
<p>
O DDTSS provê os seguintes itens para todos os idiomas:
</p>

<h5>Traduções pendentes</h5>
<p>
Uma lista de traduções pendentes. São descrições que podem ser escolhidas
livremente para serem traduzidas pelo(a) usuário(a). Exemplos:
</p>
<pre>
exim4 (priority 52)
exim4-config (priority 52)
ibrazilian (priority 47, busy)
postgresql-client (priority 47)
postgresql-contrib (priority 47)
grap (priority 45)
</pre>

<p>
Um time de tradução deve tentar traduzir os pacotes de alta prioridade em
primeiro lugar (a prioridade é calculada usando-se categorias, por exemplo,
essential, base, ...). Os pacotes já estão classificados nesse sentido.
</p>

<p>
Descrições marcadas por busy (ocupada) já estão reservadas por um(a) usuário(a)
e não podem ser selecionadas por 15 minutos. Se não houver uma alteração
(commit) durante este tempo, a descrição é marcada como disponível.
</p>

<p>
A descrição precisa ser completamente traduzida até que a interface a aceite.
Por favor, certifique-se de que tem tempo hábil para traduzir todo o
texto antes de começar. Selecione <q>Submit</q> para adicionar sua tradução,
<q>Abandon</q> se você decidir não traduzi-la, ou <q>Unfetch</q> se a descrição
não deve ser traduzida (por exemplo, porque precisa ser corrigida primeiro).
Também é possível que você tenha sorte e já exista uma tradução para uma versão
anterior do modelo em inglês, juntamente com um diff das alterações na tradução
em inglês que você deve integrar na sua tradução.
Você poderá então copiar e colar essa tradução anterior na parte inferior da
página e atualizá-la apropriadamente.
</p>

<p>
# Ainda não fuciona como deveria
Para evitar flutuações desagradáveis na largura do texto, sugere-se que você não
insira quebras de linha manualmente, a menos que seja necessário (como
itens em listas). As linhas são quebradas automaticamente. Lembre-se que um(a)
usuário(a) poderá adicionar ou remover pequenas partes durante a revisão, o que
poderia resultar em tamanhos de linha inconsistentes. Outra implicação negativa
é a criação de diff (diferença entre arquivos) da revisão que são difíceis de
ler.
</p>

<p>
Também é possível escolher pacotes preferidos pelo nome. Isso é útil para se
traduzir um conjunto de pacotes similares, como manpages-de, manpages-es, em
ordem e possibilitando copiar e colar traduções prévias.
</p>

<p>
É até mesmo possível buscar por pacotes já traduzidos, para melhorá-los.
</p>

<h5>Revisão pendente</h5>
<p>
Uma lista de descrições traduzidas que ainda precisam ser revistas. A lista
pode parecer da seguinte forma:
</p>

<pre>
 1. aspell-es (needs review, had 1)
 2. bookmarks (needs initial review)
 3. doc-linux-ja-html (needs initial review)
 4. doc-linux-ja-text (needs initial review)
 5. gnome-menus (needs initial review)
 6. geany (needs review, had 2)
 7. initramfs-tools (needs initial review)
 8. inn2 (needs initial review)
</pre>

<p>
Os seguintes marcadores (flags) existem:</p>
<dl>
    <dt lang="en">needs initial review:</dt>
    <dd>A versão atual desta tradução ainda não
        passou por uma revisão.</dd>

    <dt lang="en">needs review:</dt>
    <dd>A versão atual desta tradução precisa de uma revisão
        adicional, mas já passou por pelo menos uma revisão.</dd>

    <dt lang="en">reviewed:</dt>
    <dd>Esta descrição já foi revista por um(a) usuário(a) e não foi
        modificada. Outros(as) usuários(as) precisam revisá-la.</dd>

    <dt lang="en">owner:</dt>
    <dd>Esta descrição foi traduzida ou alterada durante uma revisão pelo(a)
        usuário(a). Outros(as) usuários(as) precisam revisá-la.</dd>
</dl>

<p>
Se uma revisão com correções já aconteceu, você receberá um colorido e agradável
diff (diferença entre arquivos) que mostrará todas as alterações da última
revisão, uma vez que você selecione o pacote.
</p>

<h5>Recentemente traduzidas</h5>
<p>
Uma lista de descrições transferidas para o DDTP. Pelo menos vinte pacotes são
listados, junto a datas de transferência.
</p>

<toc-add-entry name="rules">Regras comuns de tradução</toc-add-entry>
<p>
É importante que você não altere as descrições em inglês durante a tradução. Se
você perceber algum erro nelas, envie um relatório de bug contra o pacote
correspondente, veja <a href="$(HOME)/Bugs/Reporting">Como reportar um bug no
Debian</a> para mais detalhes.
</p>

<p>
Traduza as partes não traduzidas para cada anexo que estejam marcadas por
&lt;trans&gt;. É importante que você não altere as linhas contendo somente um
ponto na primeira coluna. Isto é um separador de parágrafo e não ficará
visível nas interfaces APT.
</p>

<p>
Parágrafos que já estão traduzidos foram reutilizados de outras descrições ou de
traduções anteriores (e isto indica que este parágrafo original em inglês não
foi alterado desde aquele tempo). Uma vez que você modifique este parágrafo, ele
não alterará as outras descrições que possuam o mesmo parágrafo.
</p>

<p>
Perceba também que cada time de tradução possui suas próprias preferências, como
vocabulários e estilos de citação. Por favor, siga essas padronizações de perto.
As regras mais importantes estão publicadas
<a href="https://wiki.debian.org/Brasil/Traduzir/DDTP">nesta página wiki</a> do
time de tradução do português brasileiro. Sugere-se que se inicie revisando
traduções já existentes, ou via <a href="#DDTSS">DDTSS</a> ou navegando pelas
descrições nos sistemas de gerenciamento de pacotes como
<a href="https://packages.debian.org/aptitude">aptitude</a>, para primeiramente
pegar o jeito das preferências de tradução. Se ficar inseguro(a), entre em
contato com o time de tradução do português brasileiro pela
<a href="https://lists.debian.org/debian-l10n-portuguese">lista de discussão</a>.
</p>

<toc-add-entry>Revisão e correção de erros</toc-add-entry>
# Sugestões gerais de revisão, não específicas ao DDTSS
<p>
Atualmente, somente o DDTSS implementa um modo de revisão e somente envia essas
traduções para o DDTP se passarem por um número fixo de reviões.
</p>

<p>
Se erros comuns forem encontrados, como erros de digitação, ou outros que são
fáceis de consertar, como problemas de codificação, é possível contornar os
processos de revisão e consertá-los em todos os pacotes numa única levada
através do uso de scripts. Sugere-se que somente um(a) coordenador(a) de
tradução reúna todos os problemas e aplique o script.
</p>

<p>
Considerando que revisões podem levar muito tempo (especialmente se pequenas
correções vão sendo feitas o tempo todo), pode ser uma opção ignorar erros
simples de digitação e inconsistências durante a revisão e, posteriormente,
verificá-los todos (se tiverem sido reunidos). Isto acelera a revisão
e permite aplicar essas correções posteriormente em
todas as descrições.
</p>

<toc-add-entry>Uso de traduções</toc-add-entry>
<p>
O suporte para descrições traduzidas de pacotes está disponível desde o
<a href="https://packages.debian.org/lenny/admin/apt">lenny</a> no pacote APT.
Ao usar este pacote, todo(a) usuário(a) pode ler as descrições no seu idioma
preferido em todos os programas que usam APT. Isto inclui <tt>apt-cache</tt>,
<tt>aptitude</tt>, <tt>synaptic</tt> e vários outros.
</p>

<p>
APT faz o download de arquivos <tt>Translation-<var>idioma</var></tt> de
espelhos Debian. Os arquivos estão disponíveis somente para lenny e novas
distribuições. A localização destes arquivos nos espelhos é
<a href="http://ftp.br.debian.org/debian/dists/sid/main/i18n/">dists/main/sid/i18n/</a>.
</p>

<p>
Também é possível desabilitar o uso de traduções. Para conseguir isso, somente
adicione
</p>
<pre>
APT::Acquire::Translation "none";
</pre>
<p>
para <tt>/etc/apt/apt.conf</tt>. Em vez de <tt>none</tt>, códigos de idioma
também são suportados.
</p>

<toc-add-entry name="todo">Lista de tarefas</toc-add-entry>

<p>
Mesmo com tanto progresso no DDTP, ainda existe muito a ser feito:
</p>
<ul>
  <li>Todos os times de tradução buscam tradutores(as) e revisores(as)
      continuamente, pois precisam de ajuda com as listas realmente grandes de
      pacotes.</li>
  <li>Adição de suporte para melhorar a descrição de pacotes em inglês durante o
      processo de tradução/revisão. Talvez isto possa ser feito pela adição de
      um novo pseudoidioma inglês contendo a descrição melhorada como tradução,
      e automaticamente preencher um relatório de bug após uma revisão realizada
      com sucesso.</li>
</ul>
<p>
Você pode encontrar
<a href="https://salsa.debian.org/l10n-team/ddtp/-/issues">uma lista mais abrangente no Salsa</a>,
onde você também pode adicionar suas idéias e sugestões.
</p>
