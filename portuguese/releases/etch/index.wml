#use wml::debian::template title="Informações de lançamento do Debian &ldquo;etch&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="28d2aaba8d42e9ca9808154a8580598385f19912"

<p>O Debian GNU/Linux <current_release_etch> foi
lançado em <a href="$(HOME)/News/<current_release_newsurl_etch/>"><current_release_date_etch></a>.
O Debian 4.0 foi lançado inicialmente em <:=spokendate('2007-04-08'):>.

O lançamento incluiu várias grandes mudanças, descritas em
nosso <a href="$(HOME)/News/2007/20070408">comunicado à imprensa</a> e
nas <a href="releasenotes">notas de lançamento</a>.</p>

<p><strong>O Debian GNU/Linux 4.0 foi substituído pelo
<a href="../lenny/">Debian GNU/Linux 5.0 (<q>lenny</q>)</a>.
As atualizações de segurança foram descontinuadas no final de fevereiro de 2010.
</strong></p>

<p>Para obter e instalar o Debian, veja a página de informações de instalação e
o guia de instalação. Para atualizar a partir de uma versão mais antiga do
Debian, veja as instruções nas <a href="releasenotes">notas de lançamento</a>.</p>

<p>As seguintes arquiteturas de computadores são suportadas nesta versão:</p>

<ul>
<li><a href="../../ports/alpha/">Alpha</a>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/arm/">ARM</a>
<li><a href="../../ports/hppa/">HP PA-RISC</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/sparc/">SPARC</a>
</ul>

<p>Apesar dos nossos desejos, podem existir alguns problemas nesta versão,
embora ela tenha sido declarada <em>estável (stable)</em>. Fizemos
<a href="errata">uma lista dos problemas conhecidos mais importantes</a>,
e você sempre pode relatar outros problemas para nós.</p>
