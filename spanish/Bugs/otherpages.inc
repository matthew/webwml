<!--translation 1.1-->

<p>Otras páginas del sistema de seguimiento de fallos (BTS en 
inglés):

<ul>
  <li><a href="./">Página principal del sistema de seguimiento de fallos de Debian.</a>
  <li><a href="Reporting">Instrucciones sobre cómo informar de un fallo.</a>
  <li><a href="Access">Acceso a los registros del sistema de seguimiento de fallos.</a>
  <li><a href="Developer">Información para desarrolladores sobre el sistema de seguimiento
       de fallos.</a>
  <li><a href="server-control">Información para desarrolladores sobre cómo manipular
       fallos utilizando la interfaz de correo electrónico.</a>
  <li><a href="server-refcard">Tarjeta de referencia de los servidores de correo.</a>
  <li><a href="server-request">Cómo pedir informes de fallos por correo electrónico.</a>
#  <li><a href="db/ix/full.html">Lista completa de informes de fallos
#       pendientes y recientes.</a>
#  <li><a href="db/ix/packages.html">Paquetes con informes de fallos.</a>
#  <li><a href="db/ix/maintainers.html">Responsables de paquetes con informes 
#       de fallos.</a>
</ul>

