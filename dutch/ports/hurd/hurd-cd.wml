#use wml::debian::template title="Debian GNU/Hurd --- Hurd-cd's" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/hurd/menu.inc"
#use wml::debian::translation-check translation="38b1ccf880dcaca37d7f9783213ea20cb1dc68b8"

<define-tag cdserie>L1</define-tag>
<define-tag cdbasetarball>gnu-2009-10-18.tar.gz</define-tag>
<define-tag cdbasename>debian-<cdserie>-hurd-i386</define-tag>

<h1>Debian GNU/Hurd</h1>

<p>Hoewel veel mensen het GNU-systeem GNU/Hurd noemen, is dit niet helemaal
waar. De kernel is GNU Mach en niet de Hurd. De Hurd is een reeks servers die
bovenop de microkernel GNU Mach draaien. Zowel de Hurd als GNU Mach maken deel
uit van het GNU-project, terwijl de Linux kernel een onafhankelijk project is.</p>

<p>De eenvoudigste (en goed geteste) methode om Debian GNU/Hurd uit
te proberen, is door een virtuele machine te gebruiken via KVM. Er zijn enkele
vooraf geïnstalleerde images beschikbaar op
<url "https://cdimage.debian.org/cdimage/ports/stable/hurd-i386/README.txt">,
maar men kan ook het installatiesysteem van Debian gebruiken voor een
installatie in KVM of op een echte computer (maar de ondersteuning van de
hardware varieert, zodat het meer aan te raden is om het eens uit te proberen
met KVM).
</p>

<h2>De installatie-cd met het Debian installatiesysteem gebruiken</h2>

<p>Een versie voor hurd-i386 van het standaard Debian installatiesysteem kan
gedownload worden van <url "https://cdimage.debian.org/cdimage/ports/stable/hurd-i386/">, en een pre-release versie voor hurd-amd64 kan gedownload
worden van <url "https://cdimage.debian.org/cdimage/ports/latest/hurd-amd64/">.
Zorg ervoor dat u het README-bestand leest dat bij de iso-images beschikbaar
is. Het installatiesysteem werkt zoals het gebruikelijke Debian
installatiesysteem voor Linux, d.w.z. automatisch behalve voor enkele
details:</p>

<ul>

<li>Zorg ervoor om wisselgeheugen te activeren, anders zal Mach problemen ervaren als u alle geheugen gebruik.</li>

<li>Koppel geen aparte partitie aan met daarop <code>/usr</code>, of anders zal het opstarten mislukken.</li>

<li>
Lees <a href="hurd-install">de aantekeningen bij een handmatige installatie</a>,
waarin enkele van de laatste configuratiestappen besproken worden.
</li>

</ul>

<p>Instructies voor het branden van de images op cd zijn te vinden in de
<a href="$(HOME)/CD/faq/">Debian CD FAQ</a>.</p>

<h2>Recentere momentopnames</h2>

<p>Enkele recentere momentopnames zijn te vinden op <url "https://cdimage.debian.org/cdimage/ports/latest/hurd-i386/"> voor i386 en <url "https://cdimage.debian.org/cdimage/ports/latest/hurd-amd64/"> voor amd64</p>

<p>Dagelijkse (niet-geteste!) momentopnames zijn te vinden op
<url "https://people.debian.org/~sthibault/hurd-i386/installer/cdimage/"> voor i386
en <url "https://people.debian.org/~sthibault/hurd-amd64/installer/cdimage/"> voor amd64. Omdat ze gebaseerd
zijn op de distributie unstable, gebeurt het vrij vaak dat u er niet echt een
systeem mee kunt installeren omwille van aan gang zijnde transities, enz.
Gebruik dus liever de momentopname waarnaar u hierboven een link vindt.</p>

<h2>Een GRUB opstartschijf maken</h2>

<p>
Indien u alleen de Hurd op uw systeem installeert, kunt u het aan het
installatiesysteem overlaten om GRUB te installeren. Indien u de Hurd
installeert naast een bestaand systeem, zult u hoogstwaarschijnlijk tussen
beide willen kunnen kiezen. Indien uw bestaand systeem Linux is, kunt u
wellicht gewoon het commando update-grub uitvoeren. Dit zal uw pas
geïnstalleerd Hurd-systeem detecteren. Anders, of indien u er niet in slaagt
het Hurd-systeem op te starten, kunt u een GRUB opstartschijf maken.</p>

<p>
Installeer het pakket grub-disk of grub-rescue-pc, welke een GRUB
diskette-image bevatten. U kunt "dd" gebruiken indien u in GNU/Linux werkt
of rawrite indien u in MS werkt.
</p>

<p>
Zorg ervoor dat u begrijpt hoe Linux, GRUB en Hurd namen toekennen aan
schijven en partities. U zult ze alle drie gebruiken en hun onderlinge
relatie kan verwarrend zijn.
</p>

<p>Hurd gebruikt andere partitienamen dan Linux, wees dus voorzichtig. IDE
harde schijven worden in volgorde genummerd, te beginnen bij hd0 voor de
primaire master en zijn slaaf hd1, gevolgd door de secundaire master hd2
en zijn slaaf hd3. Ook SCSI-schijven worden in absolute volgorde genummerd.
Ze zullen steeds sd0, sd1, enz. genoemd worden, ongeacht of de twee schijven
SCSI id 4 en 5 zijn of wat dan ook. De ervaring heeft geleerd dat cd-schijven
verraderlijk kunnen zijn. Meer hierover later.</p>

<p>Partities in Linux-stijl worden altijd sn genoemd als men de Hurd gebruikt,
waarbij n het partitienummer is. Dus de eerste partitie op de eerste IDE-schijf
zal hd0s1 zijn, de derde partitie op de tweede SCSI-schijf zal sd1s3 zijn,
enzovoort.</p>

<p>GRUB1 gebruikt nog een ander systeem om partities te benoemen. Het noemt
partities (hdN,n), maar deze keer begint men zowel voor het schijfnummer als
voor het partitienummer vanaf nul te tellen. En de schijven hebben een volgorde:
alle IDE-schijven komen eerst en de SCSI-schijven op de tweede plaats. Dus
ditmaal zal de eerste partitie op de eerste IDE-schijf (hd0,0) zijn. GRUB2
doet hetzelfde, maar voor het partitienummer begint men daar vanaf 1 te tellen.
In dit geval zal het dus (hd0,1) zijn. Om helemaal voor verwarring te zorgen kan
(hd1,2) verwijzen naar de eerste SCSI-schijf wanneer u slechts één IDE-schijf
heeft, of het kan verwijzen naar de tweede IDE-schijf. Het is dus belangrijk
dat u de verschillende namen van uw partities hebt uitgewerkt voordat u
begint.</p>

<p>Geniet van de Hurd.</p>
