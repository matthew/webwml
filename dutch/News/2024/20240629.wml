#use wml::debian::translation-check translation="e3699f036461e1416232bc8af1a6f9a475163598"
<define-tag pagetitle>Debian 12 is bijgewerkt: 12.6 werd uitgebracht</define-tag>
<define-tag release_date>2024-06-29</define-tag>
#use wml::debian::news

<define-tag release>12</define-tag>
<define-tag codename>bookworm</define-tag>
<define-tag revision>12.6</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Het Debian-project kondigt met genoegen de zesde update aan van zijn
stabiele distributie Debian <release> (codenaam <q><codename></q>).
Deze tussenrelease voegt voornamelijk correcties voor beveiligingsproblemen toe,
samen met een paar aanpassingen voor ernstige problemen. Beveiligingsadviezen
werden reeds afzonderlijk gepubliceerd en, waar beschikbaar, wordt hiernaar
verwezen.</p>

<p>Merk op dat de tussenrelease geen nieuwe versie van Debian <release> is,
maar slechts een update van enkele van de meegeleverde pakketten. Het is niet
nodig om oude media met <q><codename></q> weg te gooien. Na de installatie
kunnen pakketten worden opgewaardeerd naar de huidige versie door een
bijgewerkte Debian-spiegelserver te gebruiken.</p>

<p>Wie regelmatig updates installeert vanuit security.debian.org zal niet veel
pakketten moeten updaten, en de meeste van dergelijke updates zijn opgenomen in
de tussenrelease.</p>

<p>Nieuwe installatie-images zullen binnenkort beschikbaar zijn op de gewone
plaatsen.</p>

<p>Het upgraden van een bestaande installatie naar deze revisie kan worden
bereikt door het pakketbeheersysteem naar een van de vele HTTP-spiegelservers
van Debian te verwijzen. Een uitgebreide lijst met spiegelservers is
beschikbaar op:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Oplossingen voor diverse problemen</h2>

<p>Met deze update van stable, de stabiele distributie, worden een paar
belangrijke correcties aangebracht aan de volgende pakketten:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction aide "Reparatie voor gelijktijdig lezen van uitgebreide attributen">
<correction amavisd-new "Velerlei grensparameters die conflicterende waarden bevatten verwerken [CVE-2024-28054]; oplossing voor race-conditie in postinst">
<correction archlinux-keyring "Overschakelen naar vooraf gemaakte sleutelringen; synchroniseren met bovenstroom">
<correction base-files "Update voor de tussenrelease 12.6">
<correction bash "Herbouwen om oplossing te bieden voor verouderde Built-Using">
<correction bioawk "Parallel bouwen uitschakelen om willekeurige fouten te verhelpen">
<correction bluez "Problemen met code-uitvoering op afstand verhelpen [CVE-2023-27349 CVE-2023-50229 CVE-2023-50230]">
<correction cdo "Uitschakelen van hirlam-extensies om problemen met ICON-gegevensbestanden te voorkomen">
<correction chkrootkit "Herbouwen om oplossing te bieden voor verouderde Built-Using">
<correction cjson "Ontbrekende NULL-controles oplossen [CVE-2023-50471 CVE-2023-50472]">
<correction clamav "Nieuwe bovenstroomse stabiele release; oplossen van mogelijk probleem van heap-overloop [CVE-2024-20290] en van mogelijk probleem met commando-injectie [CVE-2024-20328]">
<correction cloud-init "Declareren van conflicts/replaces op pakket met versienummer geïntroduceerd voor bullseye">
<correction comitup "Ervoor zorgen dat het op de dienst geplaatst masker verwijderd wordt in het post-installatiescript">
<correction cpu "Precies één definitie van globalLdap opgeven in de ldap-plug-in">
<correction crmsh "Logboekmap en -bestand aanmaken bij installatie">
<correction crowdsec-custom-bouncer "Herbouwen om oplossing te bieden voor verouderde Built-Using">
<correction crowdsec-firewall-bouncer "Herbouwen tegen de versie van golang-github-google-nftables met gerepareerde ondersteuning voor de little-endian-architectuur">
<correction curl "De standaardprotocollen niet behouden wanneer deze niet meer geselecteerd zijn [CVE-2024-2004]; oplossing voor geheugenlek [CVE-2024-2398]">
<correction dar "Herbouwen om oplossing te bieden voor verouderde Built-Using">
<correction dcmtk "Terdege opruimen na wissen met purge">
<correction debian-installer "Linux kernel ABI verhogen naar 6.1.0-22; opnieuw opbouwen tegen proposed-updates">
<correction debian-installer-netboot-images "Opnieuw opbouwen tegen proposed-updates">
<correction debvm "debvm-create: login installeren; bin/debvm-waitssh: ervoor zorgen dat --timeout=N werkt; bin/debvm-run: toestaan ​​dat het wordt uitgevoerd in omgevingen zonder dat TERM is ingesteld; reparatie van resolv.conf in stretch">
<correction dhcpcd5 "privsep: berichten met een lengte van nul doorlaten; oplossing voor het probleem dat de server niet correct herstart wordt tijdens opwaarderingen">
<correction distro-info-data "Intenties kenbaar maken voor bullseye/bookworm; gegevens uit het verleden corrigeren; Ubuntu 24.10 toevoegen">
<correction djangorestframework "Ontbrekende statische bestanden terugplaatsen">
<correction dm-writeboost "Bouwfout oplossen met 6.9 kernel en backports">
<correction dns-root-data "Root hints bijwerken; verlopen beveiligingsinformatie bijwerken">
<correction dpdk "Nieuwe bovenstroomse stabiele release">
<correction ebook-speaker "Ondersteuning voor een gebruikersnaam van meer dan 8 tekens bij het opsommen van groepen">
<correction emacs "Beveiligingsoplossingen [CVE-2024-30202 CVE-2024-30203 CVE-2024-30204 CVE-2024-30205]; verouderde package-keyring.gpg vervangen door een actuele versie">
<correction extrepo-data "Gegevens van de opslagplaats bijwerken">
<correction flatpak "Nieuwe bovenstroomse stabiele release">
<correction fpga-icestorm "Compatibiliteit met yosys herstellen">
<correction freetype "Ondersteuning voor COLRv1 uitzetten, dat door de bovenstroomse ontwikkelaar onbedoeld ingeschakeld was; controle van het bestaan van een functie repareren bij het aanroepen van get_colr_glyph_paint()">
<correction galera-4 "Nieuwe bovenstroomse release met probleemoplossingen; bijwerken van de bovenstroomse sleutel voor het ondertekenen van de release; datumgerelateerde testfouten voorkomen">
<correction gdk-pixbuf "ANI: Bestanden met meerdere anih-stukken verwerpen [CVE-2022-48622]; ANI: Bestanden met meerdere INAM- of IART-stukken verwerpen; ANI: De grootte van anih-stukken valideren">
<correction glewlwyd "Mogelijke bufferoverloop tijdens validatie van FIDO2 referenties oplossen [CVE-2023-49208]; open omleiding via redirect_uri repareren [CVE-2024-25715]">
<correction glib2.0 "Een (zeldzaam) geheugenlek repareren">
<correction glibc "Oplossing om destructors altijd in omgekeerde constructorvolgorde aan te roepen terugdraaien vanwege onvoorziene compatibiliteitsproblemen met toepassingen; een DTV-beschadiging repareren als gevolg van hergebruik van een TLS-module-ID na dlclose met ongebruikte TLS">
<correction gnutls28 "Reparatie voor een certtool-crash bij het verifiëren van een certificaatketen met meer dan 16 certificaten [CVE-2024-28835]; nevenkanaal repareren in de deterministische ECDSA [CVE-2024-28834]; oplossen van een geheugenlek; twee problemen met segmentoverschrijding oplossen">
<correction golang-github-containers-storage "Herbouwen om oplossing te bieden voor verouderde Built-Using">
<correction golang-github-google-nftables "Aanpassen van de functie AddSet() aan little-endian-architecturen">
<correction golang-github-openshift-imagebuilder "Herbouwen om oplossing te bieden voor verouderde Built-Using">
<correction gosu "Herbouwen om oplossing te bieden voor verouderde Built-Using">
<correction gpaste "Conflict met oudere libpgpaste6 oplossen">
<correction gross "stack-bufferoverloop repareren [CVE-2023-52159]">
<correction hovercraft "python3-setuptools vereisen">
<correction icinga2 "Problemen met segmentoverschrijding op ppc64el oplossen">
<correction igtf-policy-bundle "CAB Forum S/MIME-beleidswijziging aanpakken; geaccumuleerde updates toepassen op vertrouwensankers">
<correction intel-microcode "Beveiligingsinperkingen [CVE-2023-22655 CVE-2023-28746 CVE-2023-38575 CVE-2023-39368 CVE-2023-43490]; beperking voor INTEL-SA-01051 [CVE-2023-45733], INTEL-SA-01052 [CVE-2023-46103], INTEL-SA-01036 [CVE-2023-45745,  CVE-2023-47855] en niet-gespecificeerde functionele problemen op verschillende Intel-processors">
<correction jose "Oplossing voor een mogelijk probleem van denial-of-service [CVE-2023-50967]">
<correction json-smart "Oplossing voor buitensporige recursie die tot stack-overloop leidt [CVE-2023-1370]; oplossing voor denial-of-service via een bewerkt verzoek [CVE-2021-31684]">
<correction kio "Bestandsverlies en mogelijke vergrendelingsproblemen op CIFS oplossen">
<correction lacme "Validatielogica van post-issuance verbeteren">
<correction libapache2-mod-auth-openidc "Reparatie voor ontbrekende invoervalidatie die tot DoS leidt [CVE-2024-24814]">
<correction libesmtp "Oudere bibliotheekversies onklaar maken en vervangen">
<correction libimage-imlib2-perl "Pakketbouw repareren">
<correction libjwt "Oplossing voor timingaanval over nevenkanaal [CVE-2024-25189]">
<correction libkf5ksieve "Het lekken van wachtwoorden in logboeken op de server voorkomen">
<correction libmail-dkim-perl "Vereiste van libgetopt-long-descriptive-perl toevoegen">
<correction libpod "Op de juiste manier omgaan  met verwijderde containers">
<correction libreoffice "Oplossen van probleem met het maken van back-upkopieën van bestanden op gekoppelde samba-shares; libforuilo.so in -core-nogui niet verwijderen">
<correction libseccomp "Ondersteuning toevoegen voor systeemaanroepen tot Linux 6.7">
<correction libtommath "Overloop van gehele getallen repareren [CVE-2023-36328]">
<correction libtool "Conflict met libltdl3-dev; controle op += operator in func_append repareren">
<correction libxml-stream-perl "Compatibiliteit met IO::Socket::SSL &gt;= 2.078 herstellen">
<correction linux "Nieuwe bovenstroomse stabiele release; ABI verhogen naar 22">
<correction linux-signed-amd64 "Nieuwe bovenstroomse stabiele release; ABI verhogen naar 22">
<correction linux-signed-arm64 "Nieuwe bovenstroomse stabiele release; ABI verhogen naar 22">
<correction linux-signed-i386 "Nieuwe bovenstroomse stabiele release; ABI verhogen naar 22">
<correction lua5.4 "debian/version-script: Extra ontbrekende symbolen voor lua 5.4.4 exporteren">
<correction lxc-templates "De optie <q>mirror</q> van lxc-debian herstellen">
<correction mailman3 "Als alternatief cron-daemon vereisen; de postgresql:// url in het script post-installation corrigeren">
<correction mksh "Samengevoegde /usr in /etc/shells verwerken; oplossing voor crash met genest bashism; argumenten voor het punt-commando corrigeren; onderscheid maken tussen niet ingesteld en leeg in `typeset -p`">
<correction mobian-keyring "Archiefsleutel van Mobian bijwerken">
<correction ms-gsl "not_null-constructors als noexcept markeren">
<correction nano "Problemen met opmaaktekenreeksen oplossen; oplossing voor <q>met --cutfromcursor kan het ongedaan maken van een uitvulling een regel opeten</q>; probleem met kwaadaardige symbolische koppeling oplossen; voorbeeldbindingen in nanorc repareren">
<correction netcfg "Routering voor netmaskers met één adres afhandelen">
<correction ngircd "De optie <q>SSLConnect</q> respecteren voor inkomende verbindingen; validering van servercertificaat op serverlinks (S2S-TLS); METADATA: oplossing voor het uitschakelen van <q>cloakhost</q>">
<correction node-babel7 "Reparatie voor het bouwen tegen nodejs 18.19.0+dfsg-6~deb12u1; toevoegen van Breaks/Replaces tegen verouderde node-babel-* pakketten">
<correction node-undici "typescript lettertypes op de juiste manier exporteren">
<correction node-v8-compile-cache "Tests verbeteren wanneer een nieuwere nodejs versie wordt gebruikt">
<correction node-zx "Enigszins excentrieke test repareren">
<correction nodejs "Enigszins excentrieke tests overslaan voor mipsel/mips64el">
<correction nsis "Niet toestaan dat niet-geautoriseerde gebruikers de map van het de-installatieprogramma wissen [CVE-2023-37378]; regressie repareren bij het uitschakelen van stub-verplaatsingen; reproduceerbaar bouwen voor arm64">
<correction nvidia-graphics-drivers "Compatibiliteit met nieuwere Linux kernel builds herstellen; pakketten overnemen van nvidia-graphics-drivers-tesla; nieuw pakket nvidia-suspend-common toevoegen; dh-dkms bouwvereisten versoepelen voor compatibiliteit met bookworm; nieuwe bovenstroomse stabiele release [CVE-2023-0180 CVE-2023-0183 CVE-2023-0184 CVE-2023-0185 CVE-2023-0187 CVE-2023-0188 CVE-2023-0189 CVE-2023-0190 CVE-2023-0191 CVE-2023-0194 CVE-2023-0195 CVE-2023-0198 CVE-2023-0199 CVE-2023-25515 CVE-2023-25516 CVE-2023-31022 CVE-2024-0074 CVE-2024-0075 CVE-2024-0078 CVE-2024-0090 CVE-2024-0092]">
<correction nvidia-graphics-drivers-tesla "Compatibiliteit met nieuwere Linux kernel builds herstellen">
<correction nvidia-graphics-drivers-tesla-470 "Compatibiliteit met nieuwere Linux kernel builds herstellen; bouwen van nvidia-cuda-mps stopzetten; nieuwe bovenstroomse stabiele release; beveiligingsoplossingen [CVE-2022-42265 CVE-2024-0074 CVE-2024-0078 CVE-2024-0090 CVE-2024-0092]">
<correction nvidia-modprobe "De overgang naar de 535-serie LTS-drivers voorbereiden">
<correction nvidia-open-gpu-kernel-modules "Updaten naar de 535-serie LTS-drivers [CVE-2023-0180 CVE-2023-0183 CVE-2023-0184 CVE-2023-0185 CVE-2023-0187 CVE-2023-0188 CVE-2023-0189 CVE-2023-0190 CVE-2023-0191 CVE-2023-0194 CVE-2023-0195 CVE-2023-0198 CVE-2023-0199 CVE-2023-25515 CVE-2023-25516 CVE-2023-31022 CVE-2024-0074 CVE-2024-0075 CVE-2024-0078 CVE-2024-0090 CVE-2024-0092]">
<correction nvidia-persistenced "Overschakelen op de 535-serie LTS-drivers; bijwerken van de lijst met ondersteunde drivers">
<correction nvidia-settings "Ook bouwen voor ppc64el; nieuwe bovenstroomse LTS-release">
<correction nvidia-xconfig "Nieuwe bovenstroomse LTS-release">
<correction openrc "Niet-uitvoerbare scripts in /etc/init.d negeren">
<correction openssl "Nieuwe bovenstroomse stabiele release; problemen met buitensporige tijdsduur oplossen [CVE-2023-5678 CVE-2023-6237], probleem met vectorregisterbeschadiging op PowerPC [CVE-2023-6129], PKCS12-decodering loopt vast [CVE-2024-0727]">
<correction openvpn-dco-dkms "Bouwen voor Linux &gt;= 6.5; de map compat-include installeren; oplossing voor refcount-onevenwicht">
<correction orthanc-dicomweb "Herbouwen om oplossing te bieden voor verouderde Built-Using">
<correction orthanc-gdcm "Herbouwen om oplossing te bieden voor verouderde Built-Using">
<correction orthanc-mysql "Herbouwen om oplossing te bieden voor verouderde Built-Using">
<correction orthanc-neuro "Herbouwen om oplossing te bieden voor verouderde Built-Using">
<correction orthanc-postgresql "Herbouwen om oplossing te bieden voor verouderde Built-Using">
<correction orthanc-python "Herbouwen om oplossing te bieden voor verouderde Built-Using">
<correction orthanc-webviewer "Herbouwen om oplossing te bieden voor verouderde Built-Using">
<correction orthanc-wsi "Herbouwen om oplossing te bieden voor verouderde Built-Using">
<correction ovn "Nieuwe bovenstroomse stabiele versie; reparatie voor onvoldoende validering van inkomende BFD-pakketten [CVE-2024-2182]">
<correction pdudaemon "python3-aiohttp vereisen">
<correction php-composer-class-map-generator "Laden van systeemvereisten afdwingen">
<correction php-composer-pcre "Ontbrekende Breaks+Replaces: toevoegen bij composer (&lt;&lt; 2.2)">
<correction php-composer-xdebug-handler "Laden van systeemvereisten afdwingen">
<correction php-doctrine-annotations "Laden van systeemvereisten afdwingen">
<correction php-doctrine-deprecations "Laden van systeemvereisten afdwingen">
<correction php-doctrine-lexer "Laden van systeemvereisten afdwingen">
<correction php-phpseclib "isPrime() en randomPrime() afschermen voor BigInteger [CVE-2024-27354]; lengte van OID in ASN1 beperken [CVE-2024-27355]; reparatie voor BigInteger getLength(); zichtbaarheidsmodificatoren van statische variabelen verwijderen">
<correction php-phpseclib3 "Laden van systeemvereisten afdwingen; isPrime() en randomPrime() afschermen voor BigInteger [CVE-2024-27354]; lengte van OID in ASN1 beperken [CVE-2024-27355]; reparatie voor BigInteger getLength()">
<correction php-proxy-manager "Laden van systeemvereisten afdwingen">
<correction php-symfony-contracts "Laden van systeemvereisten afdwingen">
<correction php-zend-code "Laden van systeemvereisten afdwingen">
<correction phpldapadmin "Compatibiliteit met PHP 8.1+ herstellen">
<correction phpseclib "Laden van systeemvereisten afdwingen; isPrime() en randomPrime() afschermen voor BigInteger [CVE-2024-27354]; lengte van OID in ASN1 beperken [CVE-2024-27355]; reparatie voor BigInteger getLength()">
<correction postfix "Nieuwe bovenstroomse stabiele release">
<correction postgresql-15 "Nieuwe bovenstroomse stabiele release; de zichtbaarheid van pg_stats_ext en pg_stats_ext_exprs items beperken tot de eigenaar van de tabel [CVE-2024-4317]">
<correction prometheus-node-exporter-collectors "Spiegelnetwerk niet negatief beïnvloeden; probleemsituatie met andere uitvoeringen van apt update oplossen">
<correction pymongo "Oplossen van probleem van lezen buiten het bereik [CVE-2024-5629]">
<correction pypy3 "C0-stuurtekens en spaties weghalen in urlsplit [CVE-2023-24329]; omzeilen van TLS-handshake-beveiligingen op gesloten sockets voorkomen [CVE-2023-40217]; tempfile.TemporaryDirectory: oplossen van symbolische-koppelingprobleem bij het opruimen [CVE-2023-6597]; zipbestand beschermen tegen decompressiebom door <q>overlap tussen aanhalingstekens</q> [CVE-2024-0450]">
<correction python-aiosmtpd "Oplossen van probleem met SMTP-smokkel [CVE-2024-27305]; probleem met niet-versleutelde STARTTLS-commando-injectie oplossen [CVE-2024-34083]">
<correction python-asdf "Het onnodig vereisen van asdf-unit-schemas verwijderen">
<correction python-channels-redis "Ervoor zorgen dat pools worden gesloten bij het sluiten van de lus in de kern">
<correction python-idna "Probleem van denial-of-service oplossen [CVE-2024-3651]">
<correction python-jwcrypto "Probleem van denial-of-service oplossen [CVE-2024-28102]">
<correction python-xapian-haystack "Het vereisen van django.utils.six weglaten">
<correction python3.11 "Oplossing voor gebruik-na-vrijgave bij het ongedaan maken van de toewijzing van een frame-object; zipbestand beschermen tegen decompressiebom door <q>overlap tussen aanhalingstekens</q> [CVE-2024-0450]; tempfile.TemporaryDirectory: oplossen van symbolische-koppelingprobleem bij het opruimen [CVE-2023-6597]; oplossing voor <q>os.path.normpath(): padafbreking bij nul bytes</q> [CVE-2023-41105]; omzeilen van TLS-handshake-beveiligingen op gesloten sockets voorkomen [CVE-2023-40217]; C0-stuurtekens en spaties weghalen in urlsplit [CVE-2023-24329]; mogelijk probleem van null pointer dereference in filleutils vermijden">
<correction qemu "Nieuwe bovenstroomse stabiele release; beveiligingsoplossingen [CVE-2024-26327 CVE-2024-26328 CVE-2024-3446 CVE-2024-3447]">
<correction qtbase-opensource-src "Regressie in patch voor CVE-2023-24607 herstellen; het gebruik van systeem-CA-certificaten vermijden wanneer dat niet gewenst is [CVE-2023-34410]; bufferoverloop oplossen [CVE-2023-37369]; oneindige lus in recursieve entiteituitbreiding voor XML oplossen [CVE-2023-38197]; bufferoverloop met bewerkt KTX-afbeeldingenbestand verhelpen [CVE-2024-25580]; HPack-overloopcontrole bij gehele getallen repareren [CVE-2023-51714]">
<correction rails "breaks en replaces declareren op verouderd pakket ruby-arel">
<correction riseup-vpn "Standaard de systeemcertificaatbundel gebruiken, waardoor de mogelijkheid om verbinding te maken met een eindpunt met behulp van het LetsEncrypt-certificaat wordt hersteld">
<correction ruby-aws-partitions "Ervoor zorgen dat het binaire pakket de bestanden partitions.json en partitions-metadata.json bevat">
<correction ruby-premailer-rails "Verwijderen van het verouderde ruby-arel als bouwvereiste">
<correction rust-cbindgen-web "Nieuw bronpakket om het bouwen van nieuwere Firefox ESR-versies te ondersteunen">
<correction rustc-web "Nieuw bronpakket om het bouwen van webbrowsers te ondersteunen">
<correction schleuder "Oplossing voor het onvoldoende valideren van argumentverwerking; oplossingen voor het importeren van sleutels uit bijlagen die door Thunderbird zijn verzonden en voor het afhandelen van e-mails zonder verdere inhoud; alleen aan het begin van een e-mail naar trefwoorden zoeken; e-mailadressen met kleine letters valideren bij het controleren van abonnees; rekening houden met de Van-kopregel om antwoordadressen te vinden">
<correction sendmail "Oplossen van probleem met SMTP-smokkel [CVE-2023-51765]">
<correction skeema "Herbouwen om oplossing te bieden voor verouderde Built-Using">
<correction skopeo "Herbouwen om oplossing te bieden voor verouderde Built-Using">
<correction software-properties "software-properties-qt: toevoegen van Conflicts+Replaces: op software-properties-kde voor soepelere upgrades vanuit bullseye">
<correction supermin "Herbouwen om oplossing te bieden voor verouderde Built-Using">
<correction symfony "Laden van systeemvereisten afdwingen; DateTypTest: ervoor zorgen dat het ingediende jaar een geaccepteerde keuze is">
<correction systemd "Nieuwe bovenstroomse stabiele release; probleem van denial-of-service oplossen [CVE-2023-50387 CVE-2023-50868]; libnss-myhostname.nss: installeren na <q>files</q>; libnss-mymachines.nss: installeren voor <q>resolve</q> en <q>dns</q>">
<correction termshark "Herbouwen om oplossing te bieden voor verouderde Built-Using">
<correction tripwire "Herbouwen om oplossing te bieden voor verouderde Built-Using">
<correction tryton-client "Alleen gecomprimeerde inhoud in geverifieerde sessies verzenden">
<correction tryton-server "<q>compressiebom</q>-aanvallen vanuit niet-geverifieerde bronnen voorkomen">
<correction u-boot "Reparatie van orion-timer voor het opstarten op sheevaplug en aanverwante platforms">
<correction uif "Ondersteuning bieden voor VLAN interfacenamen">
<correction umoci "Herbouwen om oplossing te bieden voor verouderde Built-Using">
<correction user-mode-linux "Herbouwen om oplossing te bieden voor verouderde Built-Using">
<correction wayfire "Ontbrekende vereisten toevoegen">
<correction what-is-python "breaks en replaces declareren op python-dev-is-python2; het verhaspelen van de versie in de bouwregels verhelpen">
<correction wpa "Probleem met authenticatieomzeiling oplossen [CVE-2023-52160]">
<correction xscreensaver "Waarschuwingen in verband met oude versies uitzetten">
<correction yapet "Niet EVP_CIPHER_CTX_set_key_length() aanroepen in crypt/blowfish en crypt/aes">
<correction zsh "Herbouwen om oplossing te bieden voor verouderde Built-Using">
</table>


<h2>Beveiligingsupdates</h2>


<p>Met deze revisie worden de volgende beveiligingsupdates toegevoegd aan de
stabiele release. Het beveiligingsteam heeft voor elk van deze updates
al een advies uitgebracht:</p>

<table border=0>
<tr><th>Advies-ID</th>  <th>Pakket</th></tr>
<dsa 2023 5575 webkit2gtk>
<dsa 2023 5580 webkit2gtk>
<dsa 2023 5589 nodejs>
<dsa 2024 5609 slurm-wlm-contrib>
<dsa 2024 5616 ruby-sanitize>
<dsa 2024 5618 webkit2gtk>
<dsa 2024 5619 libgit2>
<dsa 2024 5620 unbound>
<dsa 2024 5621 bind9>
<dsa 2024 5623 postgresql-15>
<dsa 2024 5624 edk2>
<dsa 2024 5625 engrampa>
<dsa 2024 5626 pdns-recursor>
<dsa 2024 5627 firefox-esr>
<dsa 2024 5628 imagemagick>
<dsa 2024 5630 thunderbird>
<dsa 2024 5631 iwd>
<dsa 2024 5632 composer>
<dsa 2024 5633 knot-resolver>
<dsa 2024 5635 yard>
<dsa 2024 5637 squid>
<dsa 2024 5638 libuv1>
<dsa 2024 5640 openvswitch>
<dsa 2024 5641 fontforge>
<dsa 2024 5642 php-dompdf-svg-lib>
<dsa 2024 5643 firefox-esr>
<dsa 2024 5644 thunderbird>
<dsa 2024 5645 firefox-esr>
<dsa 2024 5646 cacti>
<dsa 2024 5650 util-linux>
<dsa 2024 5651 mediawiki>
<dsa 2024 5653 gtkwave>
<dsa 2024 5655 cockpit>
<dsa 2024 5657 xorg-server>
<dsa 2024 5658 linux-signed-amd64>
<dsa 2024 5658 linux-signed-arm64>
<dsa 2024 5658 linux-signed-i386>
<dsa 2024 5658 linux>
<dsa 2024 5659 trafficserver>
<dsa 2024 5661 php8.2>
<dsa 2024 5662 apache2>
<dsa 2024 5663 firefox-esr>
<dsa 2024 5664 jetty9>
<dsa 2024 5665 tomcat10>
<dsa 2024 5666 flatpak>
<dsa 2024 5669 guix>
<dsa 2024 5670 thunderbird>
<dsa 2024 5672 openjdk-17>
<dsa 2024 5673 glibc>
<dsa 2024 5674 pdns-recursor>
<dsa 2024 5677 ruby3.1>
<dsa 2024 5678 glibc>
<dsa 2024 5679 less>
<dsa 2024 5680 linux-signed-amd64>
<dsa 2024 5680 linux-signed-arm64>
<dsa 2024 5680 linux-signed-i386>
<dsa 2024 5680 linux>
<dsa 2024 5682 glib2.0>
<dsa 2024 5682 gnome-shell>
<dsa 2024 5684 webkit2gtk>
<dsa 2024 5685 wordpress>
<dsa 2024 5686 dav1d>
<dsa 2024 5688 atril>
<dsa 2024 5690 libreoffice>
<dsa 2024 5691 firefox-esr>
<dsa 2024 5692 ghostscript>
<dsa 2024 5693 thunderbird>
<dsa 2024 5695 webkit2gtk>
<dsa 2024 5698 ruby-rack>
<dsa 2024 5699 redmine>
<dsa 2024 5700 python-pymysql>
<dsa 2024 5702 gst-plugins-base1.0>
<dsa 2024 5704 pillow>
<dsa 2024 5705 tinyproxy>
<dsa 2024 5706 libarchive>
<dsa 2024 5707 vlc>
<dsa 2024 5708 cyrus-imapd>
<dsa 2024 5709 firefox-esr>
<dsa 2024 5711 thunderbird>
<dsa 2024 5712 ffmpeg>
<dsa 2024 5713 libndp>
<dsa 2024 5714 roundcube>
<dsa 2024 5715 composer>
<dsa 2024 5717 php8.2>
</table>


<h2>Verwijderde pakketten</h2>

<p>De volgende pakketten zijn verwijderd door omstandigheden waar wij geen invloed op hadden:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction phppgadmin "Veiligheidsproblemen; niet compatibel met de versie van PostgreSQL in bookworm">
<correction pytest-salt-factories "Enkel nodig voor salt, dat niet in bookworm zit">
<correction ruby-arel "Verouderd, geïntegreerd in ruby-activerecord, niet compatibel met ruby-activerecord 6.1.x">
<correction spip "Niet compatibel met de versie van PHP in bookworm">
<correction vasttrafik-cli "API ingetrokken">

</table>

<h2>Het Debian-installatiesysteem</h2>
<p>Het installatiesysteem werd bijgewerkt om de reparaties die met deze tussenrelease in stable, de stabiele release, opgenomen werden, toe te voegen.</p>

<h2>URL's</h2>

<p>De volledige lijsten met pakketten die met deze revisie gewijzigd werden:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>De huidige stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Voorgestelde updates voor de stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informatie over de stabiele distributie (notities bij de release, errata, enz.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Beveiligingsaankondigingen en -informatie:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Over Debian</h2>

<p>Het Debian-project is een samenwerkingsverband van ontwikkelaars van vrije
software die vrijwillig tijd en moeite steken in het produceren van het
volledig vrije besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, stuur een e-mail naar
&lt;press@debian.org&gt;, of neem contact met het release-team voor de stabiele
release op &lt;debian-release@lists.debian.org&gt;.</p>


