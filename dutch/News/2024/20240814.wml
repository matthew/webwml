# Status: published
# $Rev$
#use wml::debian::translation-check translation="2a1830b31e7529340bf703cd96b2b22677f850df"
<define-tag pagetitle>Beveiligingsondersteuning voor Bullseye overgedragen aan het LTS-team</define-tag>
<define-tag release_date>2024-08-14</define-tag>
#use wml::debian::news

##
## Translators should uncomment the following line and add their name
## Leaving translation at 1.1 is okay; that's the first version which will
## be added to Debian's webwml repository
##
#use wml::debian::translation-check translation="1.1" maintainer="Frans Spiesschaert"

<p>
Op 14 augustus 2024, drie jaar na de eerste release, eindigde de reguliere
beveiligingsondersteuning voor Debian 11, alias <q>Bullseye</q>. Het Debian team
voor <a href="https://wiki.debian.org/LTS/">Langetermijnondersteuning (Long
Term Support - LTS)</a> neemt de beveiligingsondersteuning over van het
veiligheids- en releaseteam.
</p>

<h2>Informatie voor gebruikers</h2>

<p>
Bullseye LTS zal ondersteund worden van 15 augustus 2024 tot 31 augustus 2026.
</p>

<p>
Wanneer mogelijk worden gebruikers aangemoedigd om hun computers op te
waarderen naar Debian 12, alias <q>Bookworm</q>, de huidige stabiele release
van Debian. Om de levenscyclus van Debian-releases gemakkelijker te onthouden,
zijn de betrokken Debian-teams het volgende schema overeengekomen: drie jaar
reguliere ondersteuning plus twee jaar Langetermijnondersteuning. Debian 12 zal
dus reguliere ondersteuning ontvangen tot 10 juni 2026 en
Langetermijnondersteuning tot 30 juni 2028, respectievelijk drie en vijf jaar
na de eerste release.
</p>

<p>
Gebruikers die Debian 11 moeten blijven gebruiken, kunnen relevante informatie
over de Langetermijnondersteuning van Debian vinden op
<a href="https://wiki.debian.org/LTS/Using">LTS/Using</a>.
Belangrijke informatie en wijzigingen met betrekking tot Bullseye LTS in het
bijzonder zijn te vinden op
<a href="https://wiki.debian.org/LTS/Bullseye">LTS/Bullseye</a>.
</p>

<p>
Debian 11 LTS-gebruikers worden uitgenodigd om zich te abonneren op de
<a href="https://lists.debian.org/debian-lts-announce/">mailinglijst met
aankondigingen</a> voor het ontvangen van meldingen over beveiligingsupdates, of
om de nieuwste adviezen te volgen via de webpagina met
<a href="https://www.debian.org/lts/security/">LTS Beveiligingsinformatie</a>.
</p>

<p>
Een paar pakketten vallen niet onder de Bullseye LTS-ondersteuning.
Niet-ondersteunde pakketten die op de machines van gebruikers zijn
geïnstalleerd, kunnen worden geïdentificeerd door het pakket <a href="https://tracker.debian.org/pkg/debian-security-support">debian-security-support</a> te
installeren. Als debian-security-support een niet-ondersteund pakket detecteert
dat voor u van cruciaal belang is, neem dan contact op met
<strong>debian-lts@lists.debian.org</strong>.
</p>

<p>
Debian en zijn LTS-team willen graag alle gebruikers, ontwikkelaars, sponsors en
andere Debian-teams bedanken die bijgedragen geleverd hebben en het mogelijk
hebben gemaaakt om de levensduur van eerdere stabiele releases te verlengen en
die Buster LTS tot een succes hebben gemaakt.
</p>

<p>
Als u afhankelijk bent van Debian LTS, overweeg dan om
<a href="https://wiki.debian.org/LTS/Development">aan te sluiten bij het team</a>,
patches te leveren, tests uit te voeren of
<a href="https://wiki.debian.org/LTS/Funding">de werkzaamheden te financieren</a>.
</p>


<h2>Over Debian</h2>

##  Usually we use that version ...
<p>
Het Debian-project werd in 1993 opgericht door Ian Murdock om een echt vrij
gemeenschapsproject te zijn. Sindsdien is het project uitgegroeid tot een van
de grootste en meest invloedrijke opensourceprojecten. Duizenden vrijwilligers
van over de hele wereld werken samen om Debian-software te creëren en te
onderhouden. Beschikbaar in 70 talen en een groot aantal computertypes
ondersteunend, noemt Debian zichzelf het <q>universele besturingssysteem</q>.
</p>

<h2>Contactinformatie</h2>

<p>Voor meer informatie kunt u terecht op de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, of u kunt een e-mail zenden
naar &lt;press@debian.org&gt;.</p>

