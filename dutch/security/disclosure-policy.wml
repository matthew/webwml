#use wml::debian::template title="Beleid inzake openbaarmaking van kwetsbaarheden in Debian"
#use wml::debian::translation-check translation="5a5aa2dd64b9f93c369cb9c01eafa0bef14dafa8"

Dit document is het openbaarmakings- en embargobeleid voor kwetsbaarheden van het Debian-project, zoals vereist door de status van Debian als CVE Numbering Authority of CVE-nummeringsautoriteit (Sub-CNA, <a href="https://cve.mitre.org/cve/cna/rules. html#section_2-3_record_management_rules">richtlijn 2.3.3</a>).
Raadpleeg ook de <a href="faq">FAQ van het beveiligingsteam</a> voor
aanvullende informatie over de veiligheidsprocedures van Debian.

<h2>Algemeen proces</h2>

<p>Het beveiligingsteam van Debian zal doorgaans binnen enkele dagen contact opnemen met de melders van een kwetsbaarheid. (Zie de FAQ over <a href="faq#contact">contact opnemen met het beveiligingsteam</a> en <a href="faq#discover">het melden van kwetsbaarheden</a>.)

<p>De meeste software die deel uitmaakt van het Debian-besturingssysteem is niet specifiek voor Debian geschreven. Het Debian besturingssysteem als geheel dient ook als basis voor andere GNU/Linux distributies. Dit betekent dat de meeste kwetsbaarheden die invloed hebben op Debian ook invloed hebben op andere distributies en, in veel gevallen, commerciële softwareleveranciers. Als gevolg hiervan moet de openbaarmaking van kwetsbaarheden worden gecoördineerd met andere partijen, niet alleen met de melder en Debian zelf.</p>

<p>Een forum voor een dergelijke coördinatie is het
<a href="https://oss-security.openwall.org/wiki/mailing-lists/distros">distros-lijst</a>.
Het Debian-beveiligingsteam verwacht dat ervaren beveiligingsonderzoekers rechtstreeks contact opnemen met de distros-lijst en de betrokken bovenstroomse projecten. Het Debian-beveiligingsteam zal waar nodig hulp bieden aan andere melders. Voordat derde partijen betrokken worden, wordt toestemming gevraagd aan de melders.</p>

<h2>Tijdlijn</h2>

<p>Zoals aan het begin vermeld, zal de ontvangstbevestiging per e-mail van het initiële rapport naar verwachting maximaal enkele dagen in beslag nemen.</p>

<p>Omdat voor het verhelpen van de meeste kwetsbaarheden in Debian-software coördinatie tussen verschillende partijen nodig is (bovenstroomse ontwikkelaars, andere distributies), varieert de tijd tussen de eerste melding van een kwetsbaarheid en de openbare bekendmaking ervan sterk, afhankelijk van de software en de betrokken organisaties.</p>

<p>De distros-lijst beperkt embargoperiodes (de tijd tussen de eerste melding en de openbaarmaking) tot twee weken. Langere periodes zijn echter niet ongewoon, met extra coördinatie voordat meldingen worden gedeeld met de distros-lijst, om leveranciers met maandelijkse of zelfs driemaandelijkse releasecycli tegemoet te komen. Het verhelpen van kwetsbaarheden in internetprotocollen kan zelfs nog langer duren, net als het ontwikkelen van pogingen om kwetsbaarheden in hardware te verhelpen met software.</p>

<h2>Embargo's vermijden</h2>

<p>Omdat besloten coördinatie de neiging heeft om veel wrijving te veroorzaken en het moeilijk maakt om de juiste deskundigen inzake de betreffende materie erbij te betrekken, zal Debian het openbaar maken van kwetsbaarheden aanmoedigen, zelfs voordat een oplossing is ontwikkeld, behalve wanneer een dergelijke aanpak duidelijk Debian-gebruikers en andere partijen in gevaar zou brengen.</p>

<p>Het Debian Beveiligingsteam zal melders van kwetsbaarheden vaak vragen om publieke bugrapporten in te dienen in de juiste bugvolgsyste(e)m(en) (zoals het <a href="../Bugs/">Debian bugvolgsysteem</a>) en waar nodig hulp bieden.</p>

<p>Er is geen embargo nodig voor een CVE-toewijzing of een creditering in een beveiligingsadvies.</p>

<h2>CVE-toewijzing</h2>

<p>Debian, als een sub-CNA, wijst alleen CVE-ID's toe voor kwetsbaarheden in Debian. Als een gerapporteerde kwetsbaarheid niet aan dit criterium voldoet en daarom buiten het bereik van de Debian-CNA valt, zal het Debian beveiligingsteam ofwel zorgen voor de toewijzing van CVE-ID's door andere CNA's, of de melder begeleiden bij het indienen van zijn eigen verzoek voor een CVE-ID.</p>

<p>Een CVE-toewijzing door de Debian-CNA zal openbaar worden gemaakt met de publicatie van het Debian beveiligingsadvies of wanneer de bug wordt ingediend in de juiste bugvolgsyste(e)m(en).</p>

<h2>Kwetsbaarheid versus gewone bug</h2>

<p>Vanwege de grote verscheidenheid aan software die deel uitmaakt van het Debian besturingssysteem, is het niet mogelijk om aan te geven wat een beveiligingslek is en wat een gewone softwarebug is. Neem bij twijfel contact op met het Debian beveiligingsteam.</p>

<h2>Bug bounty-programma</h2>

<p>Debian heeft geen bug bounty-programma. Onafhankelijke partijen kunnen melders aanmoedigen om contact met hen op te nemen over kwetsbaarheden in het Debian-besturingssysteem, maar dit wordt niet onderschreven door het Debian-project.</p>

<h2>Kwetsbaarheden in de Debian-infrastructuur</h2>

<p>Meldingen van kwetsbaarheden in de infrastructuur van Debian zelf worden op dezelfde manier behandeld. Als de kwetsbaarheid in de infrastructuur niet het gevolg is van een verkeerde configuratie, maar van een kwetsbaarheid in de gebruikte software, dan is de gebruikelijke coördinatie tussen meerdere partijen vereist, met vergelijkbare tijdsbestekken als hierboven beschreven.</p>
