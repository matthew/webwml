#use wml::debian::template title="Debian webpagina's vertalen" BARETITLE=true
#use wml::fmt::verbatim
#use wml::debian::translation-check translation="e8ffe40558025c3ab3775f58c330140e50b28277"

<p>Om de taak van vertalers zo makkelijk mogelijk te maken, worden de pagina's iets anders gegenereerd dan velen onder u gewend zijn. De webpagina's worden namelijk gegenereerd van broncode in
<a href="https://www.shlomifish.org/open-source/projects/website-meta-language/"><tt>wml</tt></a>-opmaak.
Voor elke taal is er een aparte map.
</p>

<p>Indien u van plan bent met een volledig nieuwe vertaling van de Debian website
te beginnen, lees dan de <a href="#completenew">paragraaf over het beginnen van een nieuwe
vertaling</a>.
</p>

<h2><a name="singlepages">Afzonderlijke pagina's vertalen</a></h2>

<p>We gebruiken WML om de specifieke inhoud van een pagina te scheiden van de
elementen die gemeenschappelijk zijn voor meerdere pagina's. Dit houdt in dat
men bepaalde WML-broncodebestanden moet bewerken in plaats van HTML-bestanden.
<a href="using_git">Gebruik Git</a>
om de huidige broncode te verkrijgen. U moet ten minste twee mappen ophalen: <tt>webwml/english/</tt> en <tt>webwml/<var>&lt;taalcode&gt;</var>/</tt>.</p>

<p>Om een afzonderlijke pagina uit het Engels naar uw taal te vertalen, moet het
originele wml-bestand vertaald worden en in de map van de andere taal geplaatst
worden. Het relatieve pad en de naam moeten hetzelfde zijn als van de Engelse
map, zodat de links blijven werken.</p>

<h3>Kopteksten van de vertaling</h3>
<p>Het wordt sterk aanbevolen om als vertaler een extra regel toe te voegen
aan de kopteksten na de laatste <code>#use</code>-opdracht, om de exacte
commit-hash van het originele bestand dat vertaald werd, vast te leggen,
waardoor <a href="uptodate">een latere opwaardering ervan makkelijker wordt</a>.
De regel ziet er zo uit::
<kbd>#use wml::debian::translation-check translation="<var>&lt;git_commit_hash&gt;</var>"</kbd>
Merk op dat, indien u het te vertalen bestand genereert met het hulpmiddel
<tt>copypage.pl</tt> (hetgeen we sterk aanraden), de git commit-hash automatisch
gegenereerd wordt. Het gebruik van <tt>copypage.pl</tt> wordt in de navolgende
tekst uitgelegd.
</p>

<p>Sommige vertaalteams gebruiken deze regel ook om aan te geven wie de
officiële vertaler is van elke webpagina. Wanneer u dit doet, zult u een
automatisch e-mailbericht ontvangen wanneer een pagina die u onderhoudt, in het
Engels bijgewerkt werd, zodat u voor een aangepaste vertaling kunt zorgen.
Daarvoor voegt u gewoon uw naam als onderhouder toe aan de
<code>#use</code>-regel, zodat deze er als volgt uitziet:
<kbd>#use wml::debian::translation-check translation="<var>git_commit_hash</var>" maintainer="<var>uw naam</var>"</kbd>.
Het gereedschap <tt>copypage.pl</tt> zal dit automatisch doen, als u de
omgevingsvariabele <tt>DWWW_MAINT</tt> instelt of als u aan de commandoregel de
schakeloptie <tt>-m</tt> gebruikt.
</p>

# I Removed cvs specific descriptions from here because of cvs to git transition.
# Help to update instruction if possible.
#
#<p>You also need to explain to the robot who you are, how often you
#want to get automatic mails and their content. For that, edit (or let
#your coordinator edit) the file
#webwml/<var>language</var>/international/<var>language</var>/translator.db.pl
#in the repository.  The syntax should be quite understandable, and you can
##use the file of the French team as template if it does not exist for
#your language yet. The robot can send several kinds of information, and
#for each of them, you can choose the frequency at which it will be
#sent to you. The different kinds of information are:
#</p>

#<ul>
# <li><b>summary</b>:  a summary of which documents are outdated</li>
# <li><b>logs</b>: the "cvs log" between the translated and current versions</li>
# <li><b>diff</b>: "cvs diff"</li>
# <li><b>tdiff</b>: the script will try to find the part of the translated text modified by the English patch</li>
# <li><b>file</b>: add the current version of the file to translate</li>
#</ul>

#<p>Then, for each of them, the value should be one of: 0 (never), 1 (monthly), 2 (weekly) or 3 (daily).</p>

#<p>An example could be:
#</p>

#<verbatim>
#                'Martin Quinson' => {
#                        email       => 'martin.quinson@tuxfamily.org',
#                        summary     => 3,
#                        logs        => 3,
#                        diff        => 3,
#                        tdiff       => 0,
#                        file        => 0
#                },
#</verbatim>

<p>De kopregels van de webpagina kunnen eenvoudig gegenereerd worden door het
script <tt>copypage.pl</tt> in de webwml hoofdmap te gebruiken. Het script
kopieert de pagina naar de juiste plaats, maakt zo nodig mappen en
make-bestanden aan en voegt automatisch de nodige kopregels toe. U zult een
waarschuwing krijgen mocht een te kopiëren pagina reeds bestaan in het archief,
zij het omdat de pagina uit het archief verwijderd werd (omdat deze te
verouderd was), of omdat iemand reeds een vertaling in het archief plaatste
en uw kopie ervan niet meer actueel is.
</p>

<p>Om te beginnen met het gebruik van het gereedschap <tt>copypage.pl</tt>,
configureert u het bestand <tt>language.conf</tt> in de <tt>webwml</tt> hoofdmap.
Dit bestand zal gebruikt worden om te bepalen naar welke taal u
vertaalt. Dit bestand moet maximaal uit twee regels bestaan. Op de eerste regel
staat de naam van de taal (zoals <tt>dutch</tt>) en de facultatieve tweede regel
kan de naam bevatten van de persoon die de vertaling onderhoudt. U kunt de taal
ook instellen met de omgevingsvariabele <tt>DWWW_LANG</tt>, en de
omgevingsvariabele <tt>DWWW_MAINT</tt> kunt u gebruiken om er uw naam in te
plaatsen, zodat deze toegevoegd wordt aan de kopregel van de gegenereerde
wml-bestanden als onderhouder van de vertaling. Een derde mogelijkheid is om de
taal en (facultatief) de onderhouder van de vertaling op te geven aan de
commandoregel via bijvoorbeeld <tt>-l dutch -m "Donald Duck"</tt> en het bestand
language.conf helemaal niet te gebruiken.
Het script biedt nog andere functionaliteit. Voer het commando uit zonder
argumenten om informatie daarover te verkrijgen.
</p>

<p>Nadat u bijvoorbeeld het commando <kbd>./copypage.pl <var>bestand</var>.wml</kbd>
gegeven heeft, kunt u in het bestand de originele tekst vertalen. Commentaar in
het bestand zal aangeven of er elementen zijn die niet vertaald zouden mogen
worden. Respecteer dit. Wijzig niet onnodig de opmaak. Indien er iets
gerepareerd moet worden, zal dit waarschijnlijk in het originele bestand moeten
gebeuren.</p>

<h3>Pagina compileren en publiceren</h3>

<p>Aangezien we gebruik maken van <a href="content_negotiation">onderhandelen
over de inhoud</a> (content negotiation), krijgen HTML-bestanden niet de naam
<tt><var>bestand</var>.html</tt>, maar wel <tt><var>bestand</var>.<var>&lt;taal&gt;</var>.html</tt>, waarbij <var>&lt;taal&gt;</var>
de tweelettercode voor de taal is, overeenkomstig
<a href="https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes">ISO 639</a>
(bijv. <tt>nl</tt> voor Nederlands).</p>

<p>U kunt het HTML-bestand bouwen vanuit het WML-bestand met de opdracht
<kbd>make <var>bestand</var>.<var>&lt;taal&gt;</var>.html</kbd>.
Als dat lukt, controleert u of de syntaxis volledig correct is met het commando
<kbd>weblint <var>bestand</var>.<var>&lt;taal&gt;</var>.html</kbd>.</p>

<p>OPMERKING: regelmatig worden de webpagina's automatisch vanuit de wml
broncode in git opnieuw gecompileerd op de server www-master. Voor het grootste
gedeelte is dit proces immuun voor fouten. Indien u echter een defect bestand
laat opnemen in het basisniveau van uw vertaling (bijvoorbeeld het basisbestand
index.wml), zal dit het compilatieproces ontregelen en alle andere updates aan
de website tegenhouden. Besteed dus bijzondere aandacht aan deze bestanden.</p>

<p>Zodra de pagina klaar voor gebruik is, kunt u ze toepassen in het
git-archief. Indien u er zelf toe gerechtigd bent, moet u de commits pushen
naar het <a href="https://salsa.debian.org/webmaster-team/webwml">webwml
git-archief</a>; indien niet, stuur deze dan naar <a
href="translation_coordinators">iemand met schrijftoegang tot het archief</a>.</p>

<h2><a name="completenew">Een nieuwe vertaling beginnen</a></h2>

<p>Indien u wilt beginnen met het vertalen van de webpagina's van Debian naar
een nieuwe taal, stuur ons dan een e-mail (in het Engels) naar het adres
<a href="mailto:webmaster@debian.org">webmaster@debian.org</a>.

<p>Eerst en vooral kloont u onze broncodeboom, zoals beschreven wordt <a
href="using_git">op onze introductiepagina over Git</a>.</p>

<p>Nadat u een git checkout uitgevoerd heeft, maakt u een basismap aan voor
uw vertaling, op hetzelfde niveau als english/ en de andere talen. De naam van
de map voor de vertaling moet in het Engels zijn en mag enkel bestaan uit
kleine letters (bijv. "dutch" en niet "Nederlands").</p>

<p>Kopieer de bestanden <tt>Make.lang</tt> en <tt>.wmlrc</tt> vanuit de map
english/ naar de map voor de nieuwe taal. Deze bestanden zijn essentieel om uw
vertalingen te kunnen compileren vanuit de WML-bestanden. Zij zijn zo
opgevat dat u enkel nog de volgende dingen moet aanpassen, nadat u ze naar de
map voor de nieuwe taal gekopieerd heeft:</p>

<ol>
  <li>In het bestand <tt>Make.lang</tt> moet de variabele LANGUAGE aangepast
      worden.

  <li>In het bestand <tt>.wmlrc</tt> moeten de variabelen CUR_LANG, CUR_ISO_LANG
      en CHARSET aangepast worden. Voeg de variabele CUR_LOCALE toe indien u
      deze nodig heeft voor het sorteren.

  <li>Sommige talen hebben mogelijk extra verwerking nodig om met de tekenset om
      te gaan. Dit kan gedaan worden met de wml-opties --prolog en --epilog.
      Gebruik hiervoor de variabelen WMLPROLOG en WMLEPILOG in
      <tt>Make.lang</tt>.

  <li>In het bestand <tt>webwml/Makefile</tt> uit de basismap moet de variabele
      LANGUAGES aangepast worden, opdat uw taal op www.debian.org samen met de
      andere talen gecompileerd zou worden. We geven er de voorkeur aan dat u
      deze specifieke aanpassing overlaat aan de webmasters. U zou het namelijk
      niet weten mocht uw vertaling defect blijken te zijn bij het opnieuw
      laden ervan uit het VCS-systeem. Maar dit zou het bouwproces van
      de rest van onze website wel defect maken.
</ol>

<p>Als dat gedaan is, neemt u de volgende regel op in een nieuw bestand genaamd
"Makefile" in die map:

<pre>
<protect>include $(subst webwml/<var>de_map_voor_uw_taal</var>,webwml/english,$(CURDIR))/Makefile</protect>
</pre>

<p>(Vervang <var>de_map_voor_uw_taal</var> door de naam van de map
die uw taal bevat.)</p>

<p>Maak nu binnen de map voor uw taal een onderliggende map met de naam "po"
en kopieer hetzelfde Makefile-bestand naar die onderliggende map
(<kbd>cp ../Makefile .</kbd>).
</p>

<p>In de map po/ voert u het commando <kbd>make init-po</kbd> uit om de initiële set *.po-bestanden aan te maken.</p>

<p>Nu u het raamwerk opgezet heeft, kunt u beginnen uw vertalingen
in te voegen in de gedeelde WML-tags welke in sjablonen gebruikt worden.
De eerste sjablonen welke u zou moeten vertalen, zijn deze die op alle
webpagina's voorkomen, zoals de trefwoorden uit de koptekst, de elementen
uit de navigatiebalk, en de voettekst.</p>

# The page on <a href="using_wml">using WML</a> has more information on this.

<p>Begin met de vertaling van het bestand <code>po/templates.<var>xy</var>.po</code>,
(waarbij <var>xy</var> de tweelettercode is voor uw taal). Voor elke
<code>msgid "<var>iets</var>"</code> is er initieel een
<code>msgstr ""</code> waar u de vertaling van <var>iets</var> tussen de dubbele
aanhalingstekens na <code>msgstr</code> kunt invullen.</p>

<p>Het is niet nodig alle tekenreeksen in alle .po-bestanden te vertalen, enkel deze
welke u voor de momenteel vertaalde pagina's nodig heeft. Om te weten te komen of
het nodig is om een tekenreeks te vertalen, kijkt u naar het commentaar in het .po-bestand
net boven elke <code>msgid</code>-frase. Indien het bestand
waarnaar verwezen wordt, zich bevindt in <tt>english/template/debian</tt>, dan
zult u deze hoogstwaarschijnlijk moeten vertalen. Zo niet, dan
kunt u de vertaling ervan tot een later moment uitstellen, wanneer u daadwerkelijk de
betreffende sectie van de webpagina's die dit bestand nodig hebben, vertaalt.</p>

<p>Het doel van .po-bestanden is om de zaken makkelijker te maken voor
vertalers, zodat ze (bijna) nooit iets moeten bewerken in de map
<tt>english/template/debian</tt> zelf.
Indien u iets vindt dat op een foutieve wijze opgezet werd in de
map templates, zorg er dan voor dat dit probleem op een algemene manier
aangepakt wordt (vraag gerust iemand anders om dit voor u te doen),
in plaats van vertalingen in de sjablonen vast te leggen, wat
(gewoonlijk) een groter probleem zou opleveren.</p>

<p>Indien u twijfelt of u iets op de juiste manier gedaan heeft, vraag dit dan op
de debian-www mailinglijst voordat u iets vastlegt in het git-archief.</p>

<p>Opmerking: indien u het nodig vindt nog meer aanpassingen te maken,
stuur dan een e-mail naar debian-www waarin u uitlegt wat u veranderde en waarom,
zodat het probleem gecorrigeerd kan worden.</p>

<p>Nadat u klaar bent met het sjabloonraamwerk, kunt u beginnen met het vertalen
van de startpagina en de andere *.wml-bestanden. Om een lijst te zien van eerst
te vertalen pagina's, raadpleegt u <a href="translation_hints">de pagina met
suggesties</a>. Vertaal *.wml-pagina's zoals beschreven
<a href="#singlepages">bovenaan deze pagina</a>.</p>

<h3>Verouderde vertalingen nieuw leven inblazen</h3>

<p>Zoals beschreven wordt in <a href="uptodate">hoe vertalingen actueel houden</a>,
kunnen verouderde vertalingen van de website automatisch verwijderd worden na
een lange periode zonder enige actualisering.</p>

<p>Indien u vindt dat bepaalde bestanden ooit in het verleden verwijderd werden,
welke u nu terug zou willen opvragen om deze verder te bewerken, kunt u zoeken
in de commit-geschiedenis met behulp van de standaardcommando's van git.</p>

<p>Bijvoorbeeld, als het verwijderde bestand "verwijderd.wml" heette, kunt u de
geschiedenis doorzoeken met het commando:</p>

<verbatim>
   git log --all --full-history -- <pad/naar/bestand/verwijderd.wml>
</verbatim>

<p>U kunt de exacte vastlegging (commit) te weten komen waarmee het bestand
verwijderd werd, samen met de hash van die vastlegging. Om gedetailleerde
informatie te zien over welke wijzigingen er in deze vastlegging aan het bestand
aangebracht werden, kunt u het subcommando <code>git show</code> gebruiken:</p>

<verbatim>
  git show <COMMIT_HASH_STRING> -- <pad/naar/bestand/verwijderd.wml>
</verbatim>

<p>Indien de vastlegging exact deze is waarmee het bestand verwijderd werd,
kunt u het bestand naar de werkruimte terughalen met het commando <code>git checkout</code>:</p>

<verbatim>
  git checkout <COMMIT_HASH_STRING>^ -- <pad/naar/bestand/verwijderd.wml>
</verbatim>

<p>Nadat u dit gedaan heeft, moet u het document uiteraard eerst bijwerken
voordat u het opnieuw incheckt, anders zou het opnieuw verwijderd kunnen
worden.</p>


<h3>De rest van het verhaal</h3>

<p>Met de bovenstaande beschrijving weet u waarschijnlijk voldoende om aan de slag te gaan. Later zult u wellicht de volgende documenten willen naslaan waarin u meer gedetailleerde uitleg en bijkomende nuttige informatie kunt vinden.</p>

<ul>
<li>Er staat een aantal <a href="examples">voorbeelden</a> ter beschikking
    waarmee u een duidelijker beeld kunt krijgen hoe u van start kunt gaan.
<li>Een aantal gangbare vragen krijgt een antwoord en nuttige hints worden
    aangereikt in de pagina met <a href="translation_hints">vertalingshints</a>.
<li>We hebben mechanismes opgezet om te helpen bij het
    <a href="uptodate">up-to-date houden van vertalingen</a>.
<li>Om zicht te hebben op de status van uw vertaling en om een vergelijking te
    zien met andere, kunt u de pagina met <a href="stats/">statistieken</a>
    bekijken.
</ul>

<P>We hopen dat u zult vinden dat het werk dat we geleverd hebben, u het
vertalen van de pagina's zo makkelijk mogelijk maakt. Zoals eerder aangegeven,
kunt u met eventuele vragen terecht op de mailinglijst <a
href="mailto:debian-www@lists.debian.org">debian-www</a>.
