#use wml::debian::translation-check translation="4f8ee0d8b39126e64615a5dccbc22eb01dc4ce32"
<define-tag pagetitle>Release van Debian Installer Bookworm Alpha 1</define-tag>
<define-tag release_date>2022-09-22</define-tag>
#use wml::debian::news

<p>
Het Debian Installer-<a
href="https://wiki.debian.org/DebianInstaller/Team">team</a> is verheugd de eerste alpha-release van het installatieprogramma voor Debian 12 <q>Bookworm</q> aan te kondigen.
</p>

<p>
Een aantal wijzigingen werden ingediend door Debian Janitor en samengevoegd in de vele componenten waaruit het installatiesysteem is samengesteld. Ze werden niet afzonderlijk gedocumenteerd omdat ze meestal gaan over aanpassingen in functie van debhelper en andere goede praktijken tijdens het bouwen van het installatiesysteem.
</p>

<p>
Speciale dank aan de mensen die voor het eerst hebben bijgedragen aan het hoofdbronpakket van debian-installer:
</p>

<ul>
  <li>Youngtaek Yoon</li>
  <li>Sophie Brun</li>
  <li>Roland Clobus</li>
</ul>


<h2>Verbeteringen in deze release</h2>

<ul>
  <li>alsa-lib:
  <ul>
    <li>Ontbrekende map <code>/usr/share/alsa/ctl</code> installeren in libasound2-udeb
    (<a href="https://bugs.debian.org/992536">#992536</a>).</li>
  </ul>
  </li>
  <li>anna:
  <ul>
    <li>Mogelijk maken om de installatie uit te voeren met een niet-overeenkomende kernel (<a href="https://bugs.debian.org/998668">#998668</a>).</li>
  </ul>
  </li>
  <li>apt-setup:
  <ul>
    <li>ca-certificates installeren wanneer het gedetecteerde protocol https is, zodat de doelserver certificaten kan valideren (<a href="https://bugs.debian.org/1015887">#1015887</a>).</li>
  </ul>
  </li>
  <li>brltty:
  <ul>
    <li>Het hoofdmenu en debconf beëindigen, anders blijven de grafische versies achter en vullen ze de logs.</li>
    <li>Een passende menutitel toevoegen.</li>
    <li>Schermlezer inschakelen in Cinnamon (<a href="https://bugs.debian.org/992169">#992169</a>).</li>
    <li>Ondersteuning voor liblouis en hid uitschakelen in udeb.</li>
    <li>Bijwerken van de udev-regels.</li>
    <li>De breedte automatisch verkleinen tot 80 kolommen, omdat dit veel handiger is op braille-apparaten.</li>
  </ul>
  </li>
  <li>busybox:
  <ul>
    <li>Applets inschakelen voor het installatieprogramma: awk, base64, less (<a href="https://bugs.debian.org/949626">#949626</a>),
    stty (<a href="https://bugs.debian.org/891806">#891806</a>).</li>
  </ul>
  </li>
  <li>cdebconf:
  <ul>
    <li>text: ervoor zorgen dat stappen onderbroken kunnen worden  (<a href="https://bugs.debian.org/998424">#998424</a>).</li>
    <li>text: gebruik maken van libreadline en geschiedenis om kiezen met pijlen mogelijk te maken.</li>
  </ul>
  </li>
  <li>cdrom-detect:
  <ul>
    <li>Ondersteuning bieden voor het detecteren van installatie-images op gewone schijven (<a href="https://bugs.debian.org/851429">#851429</a>).</li>
  </ul>
  </li>
  <li>choose-mirror:
  <ul>
    <li>De spiegelserverlijst ophalen van mirror-master.debian.org</li>
    <li>Bij het sorteren eerst deb.debian.org nemen, daarna ftp*.*.debian.org en dan de andere.</li>
  </ul>
  </li>
  <li>console-setup:
  <ul>
    <li>De vertaling van X-symbolen naar kernelsymbolen corrigeren voor hoge Unicode-codepunten (<a href="https://bugs.debian.org/968195">#968195</a>).</li>
  </ul>
  </li>
  <li>debian-installer:
  <ul>
    <li>Spraaksyntese automatisch starten na een wachttijd van 30 seconden.</li>
    <li>Ondersteuning voor meerdere componenten toevoegen in UDEB_COMPONENTS.</li>
    <li>Linux kernel ABI verhogen naar 5.19.0-1.</li>
    <li>bookworm installeren met behulp van udebs uit bookworm.</li>
    <li>FTBFS op armel en mipsel, waar libcrypto3-udeb libatomic1 vereist, omzeilen door bestanden van de host te kopiëren.</li>
    <li>De opstartmenu's van UEFI (grub) en BIOS (syslinux) harmoniseren: enkele labels en ook inclusieregels voor spraaksynthese.</li>
    <li>De naam van de distributie niet langer vast coderen in <code>menu.cfg</code> van syslinux.</li>
    <li>Problemen met de reproduceerbaarheid van builds oplossen.</li>
  </ul>
  </li>
  <li>debootstrap:
  <ul>
    <li>(Debian) trixie toevoegen als symbolische koppeling naar sid.</li>
    <li>usr-is-merged toevoegen aan de set vereiste pakketten in testing/unstable (zie
    <a href="https://lists.debian.org/debian-devel-announce/2022/09/msg00001.html">De transitie naar usrmerge is begonnen</a>).
    </li>
  </ul>
  </li>
  <li>espeakup:
  <ul>
    <li>Het ALSA-kaartnummer afdrukken bij het kiezen van kaarten.</li>
    <li>Het inschatten van talen aanpassen.</li>
    <li>Ondersteuning voor mbrola-stemmen toevoegen en en1mrpa en us1mrpa vermijden.</li>
    <li>De mbrola-stem die tijdens het installatieproces gebruikt wordt, installeeren samen met espeak-ng.</li>
  </ul>
  </li>
  <li>finish-install:
  <ul>
    <li>De begrijpelijkheid van het herstartscherm verbeteren (<a href="https://bugs.debian.org/982640">#982640</a>).</li>
    <li>Schermlezer inschakelen in Cinnamon (<a href="https://bugs.debian.org/992169">#992169</a>).</li>
    <li>De oude symbolische koppelingen van <code>/etc/mtab</code> dezelfde bestemming geven als die welke systemd gebruikt.</li>
  </ul>
  </li>
  <li>freetype:
  <ul>
    <li>De udeb bouwen zonder librsvg.</li>
  </ul>
  </li>
  <li>gdk-pixbuf:
  <ul>
    <li>De PNG-loader rechtstreeks in de bibliotheek bouwen.</li>
  </ul>
  </li>
  <li>glibc:
  <ul>
    <li>udeb aanpassen aan de nieuwe indeling (bijna alle symbolische koppelingen zijn verdwenen).</li>
  </ul>
  </li>
  <li>hw-detect:
  <ul>
    <li><code>/etc/pcmcia/</code> vervangen door <code>/etc/pcmciautils/</code> (<a href="https://bugs.debian.org/980271">#980271</a>).</li>
    <li>Experimentele dmraid-ondersteuning verwijderen.</li>
    <li>Het pakket opal-prd installeren op OpenPOWER machines.</li>
  </ul>
  </li>
  <li>installation-report:
  <ul>
    <li>Gedetecteerde ALSA-kaarten opnemen in het hardwarerapport.</li>
    <li>Sjabloon voor het opslaan van logs herformuleren (<a href="https://bugs.debian.org/683203">#683203</a>).</li>
  </ul>
  </li>
  <li>kmod:
  <ul>
    <li>Het genereren van een minder strikt bestand shlibs implementeren.</li>
  </ul>
  </li>
  <li>linux:
  <ul>
    <li>Meer compressiemodules doen opnemen in het hoofdinstallatiepakket
    (<a href="https://bugs.debian.org/992221">#992221</a>).</li>
    <li>udeb: essiv toevoegen aan crypto-modules (<a href="https://bugs.debian.org/973378">#973378</a>).</li>
    <li>udeb: SCSI-apparaatverwerkers toevoegen aan multipath-modules (<a href="https://bugs.debian.org/989079">#989079</a>).</li>
    <li>udeb: crc64 verplaatsen naar crc-modules en zorgen dat scsi-core-modules dit vereist.</li>
  </ul>
  </li>
  <li>localechooser:
  <ul>
    <li>Niveau-detectie repareren (<a href="https://bugs.debian.org/1011254">#1011254</a>).</li>
    <li>Oplossing voor taalherkenning wanneer een 2-lettertaal een voorvoegsel is van een 3-lettertaal.</li>
  </ul>
  </li>
  <li>lvm2:
  <ul>
    <li>Het gebruik van systemd uitschakelen in udeb (<a href="https://bugs.debian.org/1015174">#1015174</a>).</li>
  </ul>
  </li>
  <li>multipath-tools:
  <ul>
    <li>De ondersteuning door het installatieprogramma verbeteren: een standaardconfiguratiebestand en udev-regels meeleveren om het detecteren van multipath-apparaten gemakkelijker te maken.</li>
  </ul>
  </li>
  <li>nano:
  <ul>
    <li>De udeb tegen libncursesw6-udeb bouwen aangezien de ondersteuning voor S-Lang is stopgezet (<a href="https://bugs.debian.org/976275">#976275</a>).</li>
  </ul>
  </li>
  <li>net-retriever:
  <ul>
    <li>Ondersteuning voor endianness in netcfg_gateway_reachable repareren (<a href="https://bugs.debian.org/1007929">#1007929</a>).</li>
    <li>Ondersteuning toevoegen voor vooraf geconfigureerd pointopoint.</li>
    <li>Ondersteuning toevoegen voor fe80-adressen als gateway.</li>
  </ul>
  </li>
  <li>nvme:
  <ul>
    <li>nvme-cli-udeb bouwen voor gebruik in het installatieprogramma.</li>
  </ul>
  </li>
  <li>openssl:
  <ul>
    <li>ossl-modules toevoegen aan udeb van libcrypto.</li>
  </ul>
  </li>
  <li>os-prober:
  <ul>
    <li>Detectie van Windows 11 toevoegen.</li>
    <li>Ondersteuning voor meerdere initrd-paden toevoegen.</li>
    <li>Detectie van Exherbo Linux toevoegen (<a href="https://bugs.debian.org/755804">#755804</a>).</li>
    <li>Linux-kernels in omgekeerde versievolgorde sorteren als er geen bootloader-configuratiebestand wordt gevonden (<a href="https://bugs.debian.org/741889">#741889</a>).</li>
    <li>Detectie van ntfs3 (5.15+ kernels) inbouwen naast ntfs en ntfs-3g.</li>
    <li>Regressie repareren door `dmraid -r` eenmaal aan te roepen.</li>
    <li>Detectie toevoegen van Alpine's initramfs-bestanden.</li>
    <li>Het lezen van <code>/usr/lib/os-release</code> toevoegen als uitwijkmogelijkheid.</li>
  </ul>
  </li>
  <li>partman-auto:
  <ul>
    <li>Experimentele dmraid-ondersteuning verwijderen.</li>
  </ul>
  </li>
  <li>partman-base:
  <ul>
    <li>Experimentele dmraid-ondersteuning verwijderen.</li>
  </ul>
  </li>
  <li>partman-jfs:
  <ul>
    <li>Verouderde controle op de geldigheid van JFS als opstart- of rootbestandssysteem verwijderen.</li>
  </ul>
  </li>
  <li>readline:
  <ul>
    <li>Toevoegen van libreadline8-udeb en readline-common-udeb, die nodig zijn voor de cdebconf-tekstfrontend (gebruikt voor op speakup gebaseerde toegankelijkheid).</li>
  </ul>
  </li>
  <li>rescue:
  <ul>
    <li>Situaties detecteren waarin het nodig kan zijn om <code>/usr</code> aan te koppelen, en hierover een vraag stellen (<a href="https://bugs.debian.org/1000239">#1000239</a>).</li>
    <li>Aparte bestandssystemen aankoppelen met aankoppelopties van fstab (nodig bijvoorbeeld met btrfs-subvolumes).</li>
    <li>Diverse problemen met het aankoppelen van verschillende aparte bestandssystemen oplossen.</li>
    <li>Diverse aankoppelings- en ontkoppelingsbewerkingen voor <code>/target</code> herstructureren.</li>
  </ul>
  </li>
  <li>rootskel:
  <ul>
    <li>Bij het opnieuw openen van de Linux-console tty1 gebruiken in plaats van tty0, wat een oplossing biedt voor Ctrl-c.</li>
  </ul>
  </li>
  <li>s390-dasd:
  <ul>
    <li>Niet langer de verouderde optie -f doorgeven aan dasdfmt (<a href="https://bugs.debian.org/1004292">#1004292</a>).</li>
  </ul>
  </li>
  <li>s390-tools:
  <ul>
    <li>Installeren van hsci, dat gebruikt wordt om HiperSockets Converged Interfaces te tonen en te controleren.</li>
  </ul>
  </li>
  <li>systemd:
  <ul>
    <li>Aparte bouw van udeb weglaten.</li>
    <li>udev-udeb: modprobe.d-fragment gebruiken om de opname van scsi_mod.scan=sync in het installatiesysteem af te dwingen.</li>
    <li>De prioriteit van systemd-timesyncd verhogen naar standaard om ervoor te zorgen dat het standaard geïnstalleerd wordt (<a href="https://bugs.debian.org/986651">#986651</a>, <a href="https://bugs.debian.org/993947">#993947</a>).</li>
  </ul>
  </li>
  <li>wireless-regdb:
  <ul>
    <li>Reguliere bestanden die door het installatieprogramma werden  ingezet, verwijderen (<a href="https://bugs.debian.org/1012601">#1012601</a>).</li>
  </ul>
  </li>
  <li>x11-xkb-utils:
  <ul>
    <li>Oplossing bieden voor de crash van setxkbmap in het installatieprogramma (<a href="https://bugs.debian.org/1010161">#1010161</a>).</li>
  </ul>
  </li>
</ul>


<h2>Veranderingen in de hardwareondersteuning</h2>

<ul>
  <li>debian-installer:
  <ul>
    <li>armhf: ondersteuning toevoegen voor Bananapi_M2_Ultra (<a href="https://bugs.debian.org/982913">#982913</a>).</li>
    <li>armhf: MX53LOCO-bestandsnaam bijwerken met nieuwere u-boot.</li>
  </ul>
  </li>
  <li>flash-kernel:
  <ul>
    <li>flash-kernel overslaan in alle EFI-systemen.</li>
    <li>Ondersteuning toevoegen voor ODROID-C4, -HC4, -N2, -N2Plus (<a href="https://bugs.debian.org/982369">#982369</a>).</li>
    <li>Toevoegen van Librem5r4 (Evergreen).</li>
    <li>Toevoegen van SiFive HiFive Unmatched A00 (<a href="https://bugs.debian.org/1006926">#1006926</a>).</li>
    <li>Toevoegen van het bord BeagleV Starlight Beta.</li>
    <li>Toevoegen van Microchip PolarFire-SoC Icicle Kit.</li>
    <li>Toevoegen van MNT Reform 2.</li>
  </ul>
  </li>
  <li>linux:
  <ul>
    <li>arm64: panel-edp opnemen in fb-modules udeb.</li>
    <li>arm64: nvmem-rockchip-efuse en phy-rockchip-inno-hdmi toeveogen aan fb-modules udeb.</li>
    <li>arm64: pwm-imx27, nwl-dsi, ti-sn65dsi86, imx-dcss, mxsfb,
    mux-mmio en imx8mq-interconnect toevoegen aan fb-modules udeb voor de MNT Reform 2.</li>
    <li>mips*: Varianten van het installatiesysteem gelijkschakelen.</li>
    <li>mips*: een generiek platform toevoegen en 5kc-malta verwijderen uit de 32-bits-architecturen.</li>
  </ul>
  </li>
  <li>oldsys-preseed:
  <ul>
    <li>Ondersteuning weglaten voor arm*/ixp4xx en arm*/iop32x (worden niet langer ondersteund door de Linux-kernel).</li>
  </ul>
</li>
</ul>


<h2>Toestand van de lokalisatie</h2>

<ul>
  <li>78 talen worden in deze release ondersteund.</li>
  <li>Voor 30 daarvan betreft het een volledige vertaling.</li>
</ul>


<h2>Bekende problemen in deze release</h2>

<p>
Raadpleeg de <a href="$(DEVEL)/debian-installer/errata">errata</a>
voor details en een volledige lijst van bekende problemen.
</p>


<h2>Feedback over deze release</h2>

<p>
We hebben uw hulp nodig om bugs te vinden en het installatiesysteem verder te verbeteren, dus probeer het alstublieft uit. Installatie-cd's, andere media en alles wat u voorts nodig heeft, kunt u vinden op onze <a href="$(DEVEL)/debian-installer">website</a>.
</p>


<h2>Bedanking</h2>

<p>
Het Debian Installer-team bedankt iedereen die bijgedragen heeft aan deze release.
</p>
