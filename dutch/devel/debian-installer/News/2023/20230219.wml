#use wml::debian::translation-check translation="ad23dfa1abd331efbc299f6c550d72ae2f406f29"
<define-tag pagetitle>Release van Debian Installer Bookworm Alpha 2</define-tag>
<define-tag release_date>2023-02-19</define-tag>
#use wml::debian::news

<p>
Het Debian Installer-<a
href="https://wiki.debian.org/DebianInstaller/Team">team</a> is verheugd de tweede alpha-release van het installatieprogramma voor Debian 12 <q>Bookworm</q> aan te kondigen.
</p>

<p>
Naar aanleiding van de
<a href="https://www.debian.org/vote/2022/vote_003">2022 Algemene resolutie over niet-vrije firmware</a>,
zijn veel pakketten met firmwarebestanden die tijdens runtime door de Linux-kernel kunnen worden opgevraagd, verplaatst van non-free naar non-free-firmware. Hierdoor komen ze in aanmerking voor opname op officiële media conform het bijgewerkte punt #5 van het Sociaal Contract.
</p>

<p>
Vanaf deze release bevatten officiële images firmwarepakketten uit main en non-free-firmware, samen met metadata om het geïnstalleerde systeem dienovereenkomstig te configureren. Onze installatiehandleidng <a href="https://www.debian.org/releases/bookworm/amd64/ch02s02">is dienovereenkomstig bijgewerkt</a>.
</p>


<h2>Verbeteringen in deze release</h2>

<ul>
  <li>apt-setup:
  <ul>
    <li>Een sjabloon apt-setup/non-free-firmware toevoegen, vergelijkbaar met de sjablonen non-free en contrib, die gebruikt worden door 50mirror om de component non-free-firmware in te schakelen: alleen installatiemodus expert, standaard uitgeschakeld, maar automatisch ingesteld wanneer non-free firmwarepakketten worden geïnstalleerd.</li>
    <li>Factorisatie van de componentverwerking over Debian-generatoren en ondersteuning voor niet-vrije firmware toevoegen aan al deze generatoren.</li>
    <li>Voorkomen dat er aan het einde van de installatie dubbele cdrom-vermeldingen achterblijven in de vorm van commentaar (<a href="https://bugs.debian.org/1029922">#1029922</a>).</li>
  </ul>
  </li>
  <li>base-installer:
  <ul>
    <li>Multi-arch-kwalificatie negeren (<a href="https://bugs.debian.org/1020426">#1020426</a>).</li>
  </ul>
  </li>
  <li>cdebconf:
  <ul>
    <li>tekst: tijdens de voortgang ten minste elke minuut feedback geven aan de gebruiker.</li>
    <li>newt: meer items linksboven uitlijnen in braillemodus.</li>
  </ul>
  </li>
  <li>debian-cd:
  <ul>
    <li>De modalias-informatie voor firmware-sof-signed vernieuwen.</li>
    <li>Gegenereerde firmware-metagegevens uitbreiden met componentinformatie.</li>
    <li>Firmwarepakketten toevoegen op basis van de geconfigureerde componenten (bijv. main en non-free-firmware voor officiële builds).</li>
    <li>De maximale imagegrootte voor netinst CD-images verhogen naar 1G: 650 MB is niet langer voldoende, maar de huidige images zijn nog steeds kleiner dan 700 MB.</li>
    <li>Een robuustere installatie-opzoeking implementeren bij het opstarten vanaf grub-efi, met behulp van .disk/id/$UUID in plaats van met .disk/info (<a href="https://bugs.debian.org/1024346">#1024346</a>, <a href="https://bugs.debian.org/1024720">#1024720</a>).</li>
    <li>Alle amd64-installatie-images worden nu opgestart op amd64-machines met 32-bits UEFI-firmware. Gebruikers hebben het multi-arch installatiesysteem niet langer nodig voor die machines.</li>
    <li>Dienovereenkomstig ophouden met het bouwen van multi-arch images.</li>
    <li>Ondersteuning voor win32-loader, dat niet langer onderhouden wordt, verwijderen..</li>
  </ul>
  </li>
  <li>debian-installer:
  <ul>
    <li>Overschakelen naar het gebruik van de grafische ontwerpen van Bookworm (Smaragd) voor de startschermen.</li>
    <li>Reparatie voor het ontbreken van grafische lettertekens voor het Georgisch (<a href="https://bugs.debian.org/1017435">#1017435</a>).</li>
    <li>Linux kernel ABI verhogen naar 6.1.0-3.</li>
  </ul>
  </li>
  <li>debootstrap:
  <ul>
    <li>Specifieke afhandeling toevoegen voor het pakket usr-is-merged.</li>
  </ul>
  </li>
  <li>espeakup:
  <ul>
    <li>De speakup_soft direct parameter instellen op 1 in het geïnstalleerde systeem voor niet-Engelse taal, om de uitspraak van symbolen en cijfers te corrigeren.</li>
    <li>Waarschuwingen over ontbrekende mbrola-stemmen vermijden.</li>
    <li>espeakup in een lus uitvoeren om crashes te compenseren.</li>
  </ul>
  </li>
  <li>grub-installer:
  <ul>
    <li>Oplossing voor ontbrekende debconf-module-integratie.</li>
  </ul>
  </li>
  <li>grub2:
  <ul>
    <li>De uitgeschakelde sectie GRUB_DISABLE_OS_PROBER als commentaar toevoegen aan /etc/default/grub om het voor gebruikers makkelijker te maken om os-prober weer in te schakelen als ze dat willen (<a href="https://bugs.debian.org/1013797">#1013797</a>, <a href="https://bugs.debian.org/1009336">#1009336</a>).</li>
  </ul>
  </li>
  <li>hw-detect:
  <ul>
    <li>Ondersteuning toevoegen voor Contents-firmware-indexen om het opzoeken van firmware te versnellen (van gevraagde bestanden tot beschikbare pakketten), waarbij het handmatig opzoeken als terugvaloptie behouden blijft.</li>
    <li>Ondersteuning toevoegen voor componentdetectie, waardoor apt-setup/$component automatisch wordt ingeschakeld.</li>
    <li>De “usb”-module oplossen in de onderliggende module wanneer deze firmwarebestanden aanvraagt ​​(bijv. rtl8192cu voor een RTL8188CUS USB Wi-Fi-dongle), om de juiste module opnieuw te laden na het implementeren van het relevante firmwarepakket.</li>
    <li>Ondersteuning verwijderen voor het laden van udeb firmware pakketten (* .udeb, * .ude).</li>
    <li>Opmerking: gebruiksscenario's met betrekking tot het laden van firmware vanaf externe opslag moeten nog worden uitgeklaard (<a href="https://bugs.debian.org/1029543">#1029543</a>). Nu firmware-pakketten worden opgenomen in de officiële images, wordt verwacht dat dit veel minder nuttig zal zijn dan het was.</li>
    <li>De historische dans link up/link down op sommige netwerkinterfaces overslaan: terwijl deze nuttig is om ervoor te zorgen dat modules firmwarebestanden aanvragen die ze mogelijk nodig hebben, is deze echt schadelijk als de interface is geconfigureerd (bijvoorbeeld handmatig of via preseed): de link is actief en/of betrokken bij een binding.</li>
    <li>Ondersteuning implementeren voor de instelling hw-detect/firmware-lookup=never (<a href="https://bugs.debian.org/1029848">#1029848</a>).</li>
  </ul>
  </li>
  <li>libdebian-installer:
  <ul>
    <li>Multiarch-kwalificatie-suffix negeren (<a href="https://bugs.debian.org/1020783">#1020783</a>).</li>
  </ul>
  </li>
  <li>localechooser:
  <ul>
    <li>Vietnamees inschakelen in een niet-BOGL-console.</li>
  </ul>
  </li>
  <li>ifupdown:
  <ul>
    <li>Oplossing voor het probleem op van de ontbrekende configuratie voor draadloze verbindingen in /etc/network/interfaces wanneer NetworkManager niet is geïnstalleerd. Wijzigen van zijn modus naar 600 voor veilige verbindingen, aangezien er uiteindelijk geheime informatie in vervat zit (<a href="https://bugs.debian.org/1029352">#1029352</a>, eerste probleem).li>
    <li>Configuratie van /etc/network/interfaces aanpassen voor draadloze verbindingen met zowel DHCP als SLAAC: alleen een DHCP-stanza schrijven en RA's de rest laten doen tijdens runtime (<a href="https://bugs.debian.org/1029352">#1029352</a>, tweede probleem).</li>
  </ul>
  </li>
  <li>preseed:
  <ul>
    <li>De alias “firmware” toevoegen voor “hw-detect/firmware-lookup”
    (<a href="https://bugs.debian.org/1029848">#1029848</a>).</li>
  </ul>
  </li>
  <li>rootskel:
  <ul>
    <li>reopen-console-linux: wanneer speakup wordt aangevraagd, dan de voorkeur geven aan tty1, de enige console die speakup kan lezen.</li>
  </ul>
  </li>
  <li>rootskel-gtk:
  <ul>
    <li>De grafische vormgeving updaten met het thema Emerald.
    </li>
  </ul>
  </li>
  <li>user-setup:
  <ul>
    <li>De ondersteuning verwijderen voor wachtwoorden zonder dat shadow gebruikt wordt.</li>
  </ul>
  </li>
</ul>


<h2>Veranderingen in de hardwareondersteuning</h2>

<ul>
  <li>debian-installer:
  <ul>
    <li>[amd64, arm64] netboot-images bouwen voor ChromeOS-apparaten. Er zijn meer wijzigingen nodig om deze images bruikbaar te maken.</li>
  </ul>
  </li>
  <li>grub2:
  <ul>
    <li>smbios toevoegen aan de ondertekende grub efi-images (<a href="https://bugs.debian.org/1008106">#1008106</a>).</li>
    <li>serial toevoegen aan de ondertekende grub efi-images (<a href="https://bugs.debian.org/1013962">#1013962</a>).</li>
    <li>Xen binaire bestanden niet strippen zodat ze opnieuw werken (<a href="https://bugs.debian.org/1017944">#1017944</a>).</li>
    <li>EFI zboot-ondersteuning activeren op arm64 (<a href="https://bugs.debian.org/1026092">#1026092</a>).</li>
    <li>Sommige nieuwe ext2-vlaggen negeren om compatibel te blijven met de recentste standaardinstellingen van mke2fs (<a href="https://bugs.debian.org/1030846">#1030846</a>).</li>
  </ul>
  </li>
  <li>linux:
  <ul>
    <li>udeb: ledtrig-audio verplaatsen van sound-modules naar
    kernel-image</li>
    <li>udeb: Ook stuurprogramma's toevoegen in onderliggende mappen van
    drivers/net/phy</li>
    <li>[arm64] stuurprogramma nvmem-imx-ocotp toevoegen aan de udeb voor het kernel-image</li>
    <li>[arm64]  stuurprogramma imx2_wdt toevoegen aan de udeb van het kernel-image</li>
    <li>[arm64] i2c-imx toevoegen aan udeb i2c-modules</li>
  </ul>
  </li>
</ul>


<h2>Toestand van de lokalisatie</h2>

<ul>
  <li>78 talen worden in deze release ondersteund.</li>
  <li>Voor 40 daarvan betreft het een volledige vertaling.</li>
</ul>


<h2>Bekende problemen in deze release</h2>

<p>
Raadpleeg de <a href="$(DEVEL)/debian-installer/errata">errata</a>
voor details en een volledige lijst van bekende problemen.
</p>


<h2>Feedback over deze release</h2>

<p>
We hebben uw hulp nodig om bugs te vinden en het installatiesysteem verder te verbeteren, dus probeer het alstublieft uit. Installatie-cd's, andere media en alles wat u voorts nodig heeft, kunt u vinden op onze <a href="$(DEVEL)/debian-installer">website</a>.
</p>


<h2>Bedanking</h2>

<p>
Het Debian Installer-team bedankt iedereen die bijgedragen heeft aan deze release.
</p>
