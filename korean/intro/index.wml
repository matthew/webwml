#use wml::debian::template title="데비안 소개" MAINPAGE="true" FOOTERMAP="true" 
#use wml::debian::recent_list
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"
<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="community">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="community"></a>
      <h2>데비안은 커뮤니티</h2>
      <p>
      전 세계 수천 명의 자원 봉사자가 자유 및 오픈 소스 소프트웨어를 우선시하는 데비안 운영 체제에서 함께 일하고 있습니다. 
      데비안 프로젝트를 만나보세요.
      </p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="people">사람들:</a>
          우리는 누구이며 무엇을 하는가
        </li>
        <li>
          <a href="philosophy">철학:</a>
          왜 하는가 그리고 어떻게 하는가
        </li>
        <li>
          <a href="../devel/join/">참여:</a>
          데비안 기여자 되기
        </li>
        <li>
          <a href="help">기여:</a>
          어떻게 데비안을 도울 수 있나요
        </li>
        <li>
          <a href="../social_contract">데비안 사회 계약:</a>
          우리 도덕적 의제
        </li>
        <li>
          <a href="diversity">누구나 환영:</a>
          데비안 다양성 선언
        </li>
        <li>
          <a href="../code_of_conduct">참가자를 위하여:</a>
          데비안 행동강령
        </li>
        <li>
          <a href="../partners/">파트너:</a>
          데비안 프로젝트 지원하는 회사 및 조직
        </li>
        <li>
          <a href="../donations">기부:</a>
          데비안 프로젝트 후원자 되는 방법
        </li>
        <li>
          <a href="../legal/">법적 이슈:</a>
          라이선스, 상표, 개인정보 보호 정책, 특허 정책 등.
        </li>
        <li>
          <a href="../contact">연락:</a>
          우리에게 연락하는 방법
        </li>
      </ul>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-desktop fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="software"></a>
      <h2>데비안은 운영체제</h2>
      <p>데비안은 자유 운영체제이며, 데비안 프로젝트에서 개발하고 유지합니다.
      사용자 요구를 만나는 수천개 응용프로그램이 있는 자유 리눅스 배포판.
      </p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="../distrib">다운로드:</a>
          어디서 데비안을 얻나요?
        </li>
        <li>
          <a href="why_debian">왜 데비안인가:</a>
          데비안을 선택하는 까닭
        </li>
        <li>
          <a href="../support">지원:</a>
          도움을 찾을 곳
        </li>
        <li>
          <a href="../security">보안:</a>
          최근 업데이트 <br>
          <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
              @MYLIST = split(/\n/, $MYLIST);
              $MYLIST[0] =~ s#security#../security#;
              print $MYLIST[0]; }:>
        </li>
        <li>
          <a href="../distrib/packages">소프트웨어:</a>
          데비안 패키지 긴 목록 검색 및 훑어보기
        </li>
        <li>
          <a href="../doc">문서:</a>
          설치 가이드, FAQ, HOWTO, 위키 그리고 좀 더
        </li>
        <li>
          <a href="../bugs">버그 추적 시스템 (BTS):</a>
          버그 보고 방법, BTS 문서
        </li>
        <li>
          <a href="https://lists.debian.org/">메일링 리스트:</a>
          사용자, 개발자 등을 위한 메일링 리스트 모음
        </li>
        <li>
          <a href="../blends">퓨어 블렌드:</a>
          특정 요구에 대한 메타 패키지
        </li>
        <li>
          <a href="../devel">개발자 코너:</a>
          데비안 개발자가 주로 관심 갖는 정보
        </li>
        <li>
          <a href="../ports">포트/아키텍처:</a>
          다양한 CPU 아키텍처 데비안 지원
        </li>
        <li>
          <a href="search">검색:</a>
          데비안 검색엔진 사용법 정보</li>
        <li>
          <a href="cn">언어:</a>
          데비안 웹사이트 언어 설정
        </li>
      </ul>
    </div>
  </div>

</div>

