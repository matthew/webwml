msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../stattrans.pl:284
#: ../../stattrans.pl:497
msgid "Wrong translation version"
msgstr ""

#: ../../stattrans.pl:286
msgid "This translation is too out of date"
msgstr ""

#: ../../stattrans.pl:288
msgid "The original is newer than this translation"
msgstr ""

#: ../../stattrans.pl:292
#: ../../stattrans.pl:497
msgid "The original no longer exists"
msgstr ""

#: ../../stattrans.pl:476
msgid "hits"
msgstr ""

#: ../../stattrans.pl:476
msgid "hit count N/A"
msgstr ""

#: ../../stattrans.pl:600
#: ../../stattrans.pl:739
msgid "Created with <transstatslink>"
msgstr ""

#: ../../stattrans.pl:605
msgid "Translation summary for"
msgstr ""

#: ../../stattrans.pl:608
msgid "Translated"
msgstr ""

#: ../../stattrans.pl:608
#: ../../stattrans.pl:687
#: ../../stattrans.pl:761
#: ../../stattrans.pl:807
#: ../../stattrans.pl:850
msgid "Up to date"
msgstr ""

#: ../../stattrans.pl:608
#: ../../stattrans.pl:762
#: ../../stattrans.pl:808
msgid "Outdated"
msgstr ""

#: ../../stattrans.pl:608
#: ../../stattrans.pl:763
#: ../../stattrans.pl:809
#: ../../stattrans.pl:852
msgid "Not translated"
msgstr ""

#: ../../stattrans.pl:609
#: ../../stattrans.pl:610
#: ../../stattrans.pl:611
#: ../../stattrans.pl:612
msgid "files"
msgstr ""

#: ../../stattrans.pl:615
#: ../../stattrans.pl:616
#: ../../stattrans.pl:617
#: ../../stattrans.pl:618
msgid "bytes"
msgstr ""

#: ../../stattrans.pl:625
msgid "Note: the lists of pages are sorted by popularity. Hover over the page name to see the number of hits."
msgstr ""

#: ../../stattrans.pl:631
msgid "Outdated translations"
msgstr ""

#: ../../stattrans.pl:633
#: ../../stattrans.pl:686
msgid "File"
msgstr ""

#: ../../stattrans.pl:635
msgid "Diff"
msgstr ""

#: ../../stattrans.pl:637
msgid "Comment"
msgstr ""

#: ../../stattrans.pl:638
msgid "Git command line"
msgstr ""

#: ../../stattrans.pl:640
msgid "Log"
msgstr ""

#: ../../stattrans.pl:641
msgid "Translation"
msgstr ""

#: ../../stattrans.pl:642
msgid "Maintainer"
msgstr ""

#: ../../stattrans.pl:644
msgid "Status"
msgstr ""

#: ../../stattrans.pl:645
msgid "Translator"
msgstr ""

#: ../../stattrans.pl:646
msgid "Date"
msgstr ""

#: ../../stattrans.pl:653
msgid "General pages not translated"
msgstr ""

#: ../../stattrans.pl:654
msgid "Untranslated general pages"
msgstr ""

#: ../../stattrans.pl:659
msgid "News items not translated (low priority)"
msgstr ""

#: ../../stattrans.pl:660
msgid "Untranslated news items"
msgstr ""

#: ../../stattrans.pl:665
msgid "Consultant/user pages not translated (low priority)"
msgstr ""

#: ../../stattrans.pl:666
msgid "Untranslated consultant/user pages"
msgstr ""

#: ../../stattrans.pl:671
msgid "International pages not translated (very low priority)"
msgstr ""

#: ../../stattrans.pl:672
msgid "Untranslated international pages"
msgstr ""

#: ../../stattrans.pl:677
msgid "Translated pages (up-to-date)"
msgstr ""

#: ../../stattrans.pl:684
#: ../../stattrans.pl:834
msgid "Translated templates (PO files)"
msgstr ""

#: ../../stattrans.pl:685
#: ../../stattrans.pl:837
msgid "PO Translation Statistics"
msgstr ""

#: ../../stattrans.pl:688
#: ../../stattrans.pl:851
msgid "Fuzzy"
msgstr ""

#: ../../stattrans.pl:689
msgid "Untranslated"
msgstr ""

#: ../../stattrans.pl:690
msgid "Total"
msgstr ""

#: ../../stattrans.pl:707
msgid "Total:"
msgstr ""

#: ../../stattrans.pl:741
msgid "Translated web pages"
msgstr ""

#: ../../stattrans.pl:744
msgid "Translation Statistics by Page Count"
msgstr ""

#: ../../stattrans.pl:759
#: ../../stattrans.pl:805
#: ../../stattrans.pl:849
msgid "Language"
msgstr ""

#: ../../stattrans.pl:760
#: ../../stattrans.pl:806
msgid "Translations"
msgstr ""

#: ../../stattrans.pl:787
msgid "Translated web pages (by size)"
msgstr ""

#: ../../stattrans.pl:790
msgid "Translation Statistics by Page Size"
msgstr ""

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr ""

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr ""

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr ""

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr ""

