Starting results calculation at Sat Apr 15 00:02:42 2023

Option 1 "Jonathan Carter"
Option 2 "None of the above"

In the following table, tally[row x][col y] represents the votes that
option x received over option y.

                  Option
              1     2 
            ===   === 
Option 1          259 
Option 2     15       



Looking at row 2, column 1, None of the above
received 15 votes over Jonathan Carter

Looking at row 1, column 2, Jonathan Carter
received 259 votes over None of the above.

Option 1 Reached quorum: 259 > 47.3392015141785


Option 1 passes Majority.              17.267 (259/15) > 1


  Option 1 defeats Option 2 by ( 259 -   15) =  244 votes.


The Schwartz Set contains:
	 Option 1 "Jonathan Carter"



-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

The winners are:
	 Option 1 "Jonathan Carter"

-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

The total numbers of votes tallied = 279
The total numbers of votes tallied = 279
