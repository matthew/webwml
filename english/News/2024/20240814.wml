# Status: published
# $Id$
# $Rev$
<define-tag pagetitle>Security support for Bullseye handed over to the LTS team</define-tag>
<define-tag release_date>2024-08-14</define-tag>
#use wml::debian::news

##
## Translators should uncomment the following line and add their name
## Leaving translation at 1.1 is okay; that's the first version which will
## be added to Debian's webwml repository
##
# ← this one must be removed; not that one → #use wml::debian::translation-check translation="1.1" maintainer=""

<p>
As of 14 August 2024, three years after the initial release, the regular
security support for Debian 11, alias <q>Bullseye</q>, comes to an end. The
Debian <a href="https://wiki.debian.org/LTS/">Long Term Support (LTS)</a> Team
will take over security support from the Security and the Release Teams.
</p>

<h2>Information for users</h2>

<p>
Bullseye LTS will be supported from 15 August 2024 to 31 August 2026.
</p>

<p>
Whenever possible, users are encouraged to upgrade their machines to Debian 12,
alias <q>Bookworm</q>, the current Debian stable release. To make the life
cycle of Debian releases easier to remember, the related Debian teams have
agreed on the following schedule: three years of regular support plus two years
of Long Term Support. Debian 12 will receive thus regular support until 10 June
2026 and Long Term Support until 30 June 2028, three and five years after the
initial release, respectively.
</p>

<p>
Users that need to stick with Debian 11 can find relevant information about
Debian Long Term Support at
<a href="https://wiki.debian.org/LTS/Using">LTS/Using</a>.
Important information and changes regarding Bullseye LTS specifically can be
found at
<a href="https://wiki.debian.org/LTS/Bullseye">LTS/Bullseye</a>.
</p>

<p>
Debian 11 LTS users are invited to subscribe to the
<a href="https://lists.debian.org/debian-lts-announce/">announcement mailing
list</a> for receiving notifications about security updates, or to follow the
latest advisories through the
<a href="https://www.debian.org/lts/security/">LTS Security Information</a>
webpage.
</p>

<p>
A few packages are not covered by the Bullseye LTS support. Non-supported
packages installed in the users machines can be identified by installing the
<a href="https://tracker.debian.org/pkg/debian-security-support">debian-security-support</a>
package. If debian-security-support detects an unsupported package which is
critical to you, please get in touch with
<strong>debian-lts@lists.debian.org</strong>.
</p>

<p>
Debian and its LTS Team would like to thank all contributing users, developers,
sponsors and other Debian teams who are making it possible to extend the life
of previous stable releases, and who have made Buster LTS a success.
</p>

<p>
If you rely on Debian LTS, please consider
<a href="https://wiki.debian.org/LTS/Development">joining the team</a>,
providing patches, testing or
<a href="https://wiki.debian.org/LTS/Funding">funding the efforts</a>.
</p>


<h2>About Debian</h2>

##  Usually we use that version ...
<p>
The Debian Project was founded in 1993 by Ian Murdock to be a truly
free community project. Since then the project has grown to be one of
the largest and most influential open source projects.  Thousands of
volunteers from all over the world work together to create and
maintain Debian software. Available in 70 languages, and
supporting a huge range of computer types, Debian calls itself the
<q>universal operating system</q>.
</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a> or send mail to
&lt;press@debian.org&gt;.</p>

