<define-tag pagetitle>Debian Installer Trixie Alpha 1 release</define-tag>
<define-tag release_date>2024-12-31</define-tag>
#use wml::debian::news

<p>
The Debian Installer <a
href="https://wiki.debian.org/DebianInstaller/Team">team</a> is pleased to
announce the first alpha release of the installer for Debian 13
<q>Trixie</q>.
</p>


<h2>Foreword</h2>

Cyril Brulebois would like to address special thanks to Holger Wansing who has
done a tremendous job wading through many proposed changes in various
installer components, in addition to coordinating translation efforts.


<h2>Important changes in this release</h2>

<p>
Many changes have been happening during this release cycle already, and
this announcement isn't meant to be exhaustive. Instead, let's stick to
some high-level view of the most important changes.
</p>

<p>
There are major updates on the hardware support side:
</p>

<ul>
  <li>We're no longer building an installer for the armel and i386
      architectures, even if they remain in the archive at the moment.</li>
  <li>The mipsel architecture was removed from the archive last year.</li>
  <li>The riscv64 architecture is brand new!</li>
</ul>

<p>
Even if the boot screens remain to be updated, the Ceratopsian theme by
Elise Couper is making his debut in the installer.
</p>

<p>
The user-setup screens (dealing with the creation of the root user and
the first user) have received an overdue makeover.
</p>

<p>
A lot of improvements and bugfixes have made their way to components
responsible for partitioning. This includes different heuristics for
automatic partitioning (e.g. to compute the swap size), some recipes
dedicated to small disks, etc.
</p>


<h2>Localization status</h2>

<ul>
  <li>78 languages are supported in this release.</li>
  <li>Full translation for 18 of them.</li>
</ul>


<h2>Known issues in this release</h2>

<p>
See the <a href="$(DEVEL)/debian-installer/errata">errata</a> for
details and a full list of known issues.
</p>


<h2>Feedback for this release</h2>

<p>
We need your help to find bugs and further improve the installer, so please
try it. Installer CDs, other media and everything else you will need are
available at our <a href="$(DEVEL)/debian-installer">web site</a>.
</p>


<h2>Thanks</h2>

<p>
The Debian Installer team thanks everybody who has contributed to this
release.
</p>
