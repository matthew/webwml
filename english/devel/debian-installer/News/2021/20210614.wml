<define-tag pagetitle>Debian Installer Bullseye RC 2 release</define-tag>
<define-tag release_date>2021-06-14</define-tag>
#use wml::debian::news

<p>
The Debian Installer <a
href="https://wiki.debian.org/DebianInstaller/Team">team</a> is pleased to
announce the second release candidate of the installer for Debian 11
<q>Bullseye</q>.
</p>


<h2>Improvements in this release</h2>

<ul>
  <li>cdebconf:
    <ul>
      <li>Intercept the size-request event and adjust the requested
        width to a minimum of 300 pixels. This seems sufficient to
        avoid the infinite loop in GTK (<a href="https://bugs.debian.org/988786">#988786</a>). This fixes a number
        of scenarios including:
        <ul>
          <li>Hang before offering a shell in rescue mode
            (<a href="https://bugs.debian.org/987377">#987377</a>).</li>
          <li>Hang immediately with Sinhala and other languages
            (<a href="https://bugs.debian.org/987449">#987449</a>).</li>
          <li>Hang in various places with Swedish (mirror selection or
            package manager configuration).</li>
        </ul>
      </li>
      <li>Capture new-style GLib structured logging messages
        (<a href="https://bugs.debian.org/988589">#988589</a>).</li>
      <li>Align the display of info messages (e.g. “Rescue mode”) to
        the right again, since Debian (logo and name) is back on the
        left side with the Homeworld theme.</li>
      <li>Make sure info messages (e.g. “Rescue mode”) get displayed
        on the banner on the start-up screen (<a href="https://bugs.debian.org/882804">#882804</a>).</li>
    </ul>
  </li>
  <li>choose-mirror:
    <ul>
      <li>Update Mirrors.masterlist.</li>
    </ul>
  </li>
  <li>debian-cd:
    <ul>
      <li>Add brltty and espeakup to all images from netinst up, for
        users installing without a network mirror (<a href="https://bugs.debian.org/678065">#678065</a>).</li>
    </ul>
  </li>
  <li>debian-installer:
    <ul>
      <li>Bump Linux kernel ABI to 5.10.0-7.</li>
    </ul>
  </li>
  <li>grub-installer:
    <ul>
      <li>Make sure reinstalling GRUB on BIOS systems doesn't report
        an error when everything went fine (<a href="https://bugs.debian.org/988826">#988826</a>).</li>
    </ul>
  </li>
  <li>gtk+2.0:
    <ul>
      <li>Avoid a relayout loop that's likely to happen in the
        graphical installer (<a href="https://bugs.debian.org/988786">#988786</a>).</li>
    </ul>
  </li>
  <li>lowmem:
    <ul>
      <li>Allow forcing a lowmem level (e.g. lowmem=+0).</li>
      <li>Update lowmem levels for arm64, armhf, mipsel, mips64el, and
        ppc64el.</li>
    </ul>
  </li>
  <li>open-iscsi:
    <ul>
      <li>Ship the contents of the library package inside the udeb,
        instead of having the udeb depend on the library package
        (<a href="https://bugs.debian.org/987568">#987568</a>).</li>
    </ul>
  </li>
  <li>partman-base:
    <ul>
      <li>Fix and extend support for /dev/wd* devices with
        rumpdisk.</li>
    </ul>
  </li>
  <li>systemd:
    <ul>
      <li>udev-udeb: setup /dev/fd, /dev/std{in,out,err} symlinks
        (<a href="https://bugs.debian.org/975018">#975018</a>).</li>
    </ul>
  </li>
</ul>


<h2>Hardware support changes</h2>

<ul>
  <li>linux:
    <ul>
      <li>[arm64] udeb: Include mdio module for RPi4 Ethernet (<a href="https://bugs.debian.org/985956">#985956</a>).</li>
      <li>[s390x] udeb: Include standard scsi-modules containing the
        virtio_blk module (<a href="https://bugs.debian.org/988005">#988005</a>).</li>
    </ul>
  </li>
</ul>


<h2>Localization status</h2>

<ul>
  <li>78 languages are supported in this release.</li>
  <li>Full translation for 33 of them.</li>
</ul>


<h2>Known issues in this release</h2>

<p>
See the <a href="$(DEVEL)/debian-installer/errata">errata</a> for
details and a full list of known issues.
</p>


<h2>Feedback for this release</h2>

<p>
We need your help to find bugs and further improve the installer, so please
try it. Installer CDs, other media and everything else you will need are
available at our <a href="$(DEVEL)/debian-installer">web site</a>.
</p>


<h2>Thanks</h2>

<p>
The Debian Installer team thanks everybody who has contributed to this
release.
</p>
